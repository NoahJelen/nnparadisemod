0.01A (Christmas Release):
    Added 14 new doors and trapdoors
    Added 14 new buttons
    Added 14 new pressure plates
    Added santa's suit, christmas trees, and presents
    Added more wooden crafting tables
    Added golden hopper
    Added moss stone furnace
    Added 7 new fences and gates
    Added golden bars (function just like iron bars)
    Added rubies
    Added old recipe for the enchanted apple
    Added the ability to craft quartz blocks back into quartz
    Added emerald, ruby, obsidian, and redstone armor and tools

0.02A:
    Optimized the living hell out of the block registration class, forge no longer spams the output saying I registered everything twice
    Removed a bunch of unnecessary items
    Increased the armor toughness and durability of emerald, ruby, and obsidian armor
    Increased the armor toughness of redstone armor
    Changed the button models to resemble the vanilla buttons
    Added the crafting recipe for the obsidian button
    Changed brick fence recipe to prevent recipe conflict with Quark's brick wall crafting recipe
    Made some other technical optimizations
    Added diamond, emerald, quartz, redstone, and ruby crystals
    Added soul glass and pane
    Added liquid redstone
    Added glowing obsidian (from mcpe)

0.10A Test 1:
    More technical optimizations
    Added Blaze and Sugar Blocks
    Added DU Portal Blocks
    Added the Deep Underground dimension!
    Nether Fortresses can generate in the Deep Underground
    All overworld mobs can spawn in the Deep Underground
    Obsidian Wall can no longer be moved by pistons
    Crafting the redstone sword now pre-enchants it with Fire Aspect II
    Emerald armor now gives the player experience if the player is wearing a full set
    Liquid Redstone now has a sound!

0.10A Test 2:
    Added occasional tree gen to the deep underground
    Nether Forts no longer generate in the DU
    Darkened the liquid redstone texture so it's more red
    Changed Blaze block texture to a yellow-orange like color
    Added fog to the DU
    DU Portal is now a full block
    Added Emerald Rails and its powered variant
    Added mossy variant of the vanilla lever

0.10A (Update 2018):
    Added crafting recipe for Emerald Rails and its powered variant
    Added colored redstone lamps
    Changed the sugar block texture
    The sugar block is now a falling block
    The first release of my mod to have its source on github

0.11A (The Decoration and Mob Repellent Update):
    Increased the rarity of ruby ore (it's now about as rare as diamond but less rare than emerald).
    Added Silver Ore
    Added Silver Block
    Added Silver Button
    Added Silver Pressure Plate
    Added Silver Fence
    Added Silver Fence Gate
    Renamed the vanilla buttons to "Oak Button" and "Stone Button"
    Soul Glass and DU Portal now render like normal translucent blocks
    The vanilla redstone block now has the blast resistance of obsidian
    Fixed an issue with the walls appearing in the creative inventory twice with the second set being untextured. They should only appear once now
    Added Silver Door
    Added Silver Trapdoor
    Added Silver Hopper
    Added End Stone Pressure Plate
    Added End Stone Button
    Added End Stone Door
    Added End Stone Trapdoor
    Added End Stone Wall
    Added Glowing Obsidian Stairs
    Added Bedrock Stairs
    Added Obsidian Stairs
    Added Glowing Obsidian Wall
    Added Glowing Obsidian Pressure Plate
    Added Glowing Obsidian Button
    Added Glowing Obsidian Door
    Added Glowing Obsidian Trapdoor
    Added Compressed Cactus
    Added Cactus Door
    Added Cactus Trapdoor
    Added Cactus Bookshelf
    Added Cactus Fence
    Added Cactus Fence Gate
    Added Cactus Pressure Plate
    Added Compressed Cactus Chest (Normal and Trapped)
    Added Cactus tools
    Added Cactus Stick
    Added Cactus Stairs
    Added Salt Ore
    Added Salt
    Added Salt Block
    Added Salt Lamp
    Added Nether Variants of gold and silver ores
    Adjusted the blast resistance of all the blocks so they're correct
    Resolved some recipe conflicts in my own crafting recipes

0.12A (The Structures Update):
    Added the following structures to Minecraft:
        Shipwreck (has Guardian spawners in it!)
        Underwater Village
        Brick Pyramid (A spinoff of the brick pyramid from Indev, has a dungeon inside)
        Abandoned Survival Houses (has different variants depending on biome)
        Abandoned Mining Base (Extremely dangerous! Has 2 creeper spawners inside!)
        Mesa Temple (A variant of the vanilla desert temple)
        Rogue Nether Portals
        Wicker Man Monument (inspired by the song "The Wicker Man" by Iron Maiden)
        Added 3 structures created by my friend, Attie (2 shrines of his enderman, Bill, and his shulker, John, and a tank for his creeper, Jeff)
    Finally got gold generating in the nether!
    Optimized the crafting recipes for the colored redstone lamps (they also now use oredict defs for the dyes, so you could use other mod's dye for making them if they have an oredict definition)
    Optimized ore generation
    Fixed a crafting recipe bug for the silver pickaxe
    Fixed a texture error for the Brown Redstone Lamp item (it had the same texture as the Blue Redstone Lamp)
    Fixed a block model bug with the cactus trapdoor when it had "half=top" as its metadata
    Added Dirt, Sand, and Clay to the ocean floor
    The resource blocks (the ones made of silver, ruby, sugar, salt, and blaze powder) and glowing obsidian can now be used in beacon pyramids
    Addded the command blocks and Structure Block to the redstone tab of the creative inventory and added the Barrier to the building blocks tab of the creative inventory

1.0 Test 1:
    First 1.12.2 only version
    Changed Sugar Block texture (it looked exactly like the white concrete powder)
    The wooden and cactus crafting tables now have the recipe book interface
    Started an advancement tree for the mod
    Added Ender Ruby Ore and Ender Pearl Ore
    Numbers should no longer spam your output log while exploring the end
    Added Ender Pearl Block
    Added elytra runways
    Added structure voids to the redstone creative tab

1.0 Test 2:
    Added more advancements to the advancement tree
    Fixed a rendering glitch with the cactus and compressed cactus chests
    Increased the rarity of structures to prevent the minecraft world from looking trashed
    Optimized the registration of world generators
    The christmas tree leaves now properly displays its item

1.0 (Advancement Update):
    Added Treasure Chests
    Added Sky Wheels
    Made some more optimizations
    Added mini strongholds
    Moved all items in the materials creative tab to the miscellaneous creative tab as it doesn't exist anymore
    The Silver Fence Gate finally has a display name!
    Added the vanilla mob spawner to the decorations creative tab (just like in MCBE)

1.1 Test 1:
    Improved the models of many blocks
    Fixed the "Infinite Rubies" glitch with the ruby crafting recipe
    Added rusty variants of iron blocks and items
    Just like the vanilla stone pressure plate, stone-type pressure plates (ones made of the stone variants, cobblestone, moss stone, and end stone) no longer react to items.
    Fixed crafting issue with the wooden crafting table recipes outputting an oak crafting table instead of their specified wood type.
    Fixed a bug with planks (other than oak planks) outputting an oak button instead of the specified variant (this will cause some issues with the advancements!)
    Added Landmines! Be careful when playing in survival mode with this mod or you WILL get blown up!
    Added Buoys to the oceans (they're made of rusty iron)
    Added pressure plate and buttons made of grass and dirt.

1.1 Test 2:
    Improved the generation of starter houses and landmines (they only spawn on blocks that have air above them).
    Added stone and dirt landmines.
    My structures can now generate in dimensions where overworld blocks are present (like The DU and Cavern's cave dimensions).
    Added the deep void dimension
    Added void dungeons (They come in small, medium, and large variants)
    Added void towers
    Added Void Stone
    Added Void Ores
    Added Void Stone Bricks and stairs variant
    Added Polished Void Stone
    Added Void Stone Button and Void Stone Pressure Plate
    Added Void Stone Wall

1.1 Test 3:
    Fixed an annoying bug with dimension teleportation
    Added Regeneration Stone
    Added Void Pearl
    Added Void Pearl Ore
    Added Void Pearl Block
    Added a config file
    Brick Pyramids are no longer limited to ocean biomes
    The mod now shows its newest version
    The version of my mod now ships with the resource pack by default
    Added more advancements

1.1 (Resurrection Update):
    Fixed some bugs with pick-blocking the doors
    Added recipe for void furnace
    Gave Regeneration Stone, Void Pearl, Void Pearl Block, and Void Pearl Ore display names
    Added Player Head Temples and Giant Grass Blocks (must be enabled from the config)
    Crystals can no longer generate on top of liquids
    Improved world generation in the Deep Underground

1.1.1:
    Fixed a bug with diamond crystal generating in the deep void and deep underground

1.2:
    Added biomes to Deep Underground
    For performance reasons, most of the gravel generation has been removed from the deep underground dimension.
    Liquid Redstone now generates underground in lakes
    Added Glowing Ice
    Optimized the DU and DV chunk generators

1.2.1:
    Fixed a bug with custom fences not connecting to vanilla fences
    Fixed a model bug with the vanilla and moss stone furnaces
    Fixed the void furnace item showing up as a blank space
    Added crafting recipe for black dye

1.2.2:
    Added Cooked Eggs
    The mod displays its correct version again (I'm always forgetting to update the mcmod.info file, dang it!)
    Finally added slabs for Bedrock, end stone, compressed cactus, glowing obsidian, obsidian, void stone, and void stone bricks!
    Made Obsidian, Glowing Obsidian, and Bedrock Stairs immovable by pistons (they're made of immovable materials)
    Changed the mechanic of gold and silver hoppers from transferring multiple items at to tranferring items faster than regular hoppers
    Added mesa biome generation to the deep underground
    
1.3 (Cave Update):
    Improved cave generation (thank you for the idea, u/SaveThePhytoplankton!)
    Added 2 easter eggs
    Added underground villages
    Optimized ore generation
    
1.3.1:
    Optimized cave generation (it should cause less lag now)
    Optimized structure generation
    Buried Treasure will now generate 2 meters under grass instead of deep underground
    Added Underground Air (mod internal use only)
    Added formations to caves and the nether
    Added prismarine Crystal Block
    Crystals now generate in bunches in caves
    Added lava pockets in the deep underground
    
1.4 (Update 2019):
    Optimized and corrected some block models
    Added another easter egg
    Fixed several world generation bugs
    Improved world generation
    Removed smelting recipes for modded gold items (they're useless)
    Made lots of backend optimizations
    The bounding boxes of certain blocks are now changed to fit the block model
    Fixed the jungle button model
    Added glass pressure plate
    Added sandstone pressure plate
    Added red sandstone pressure plate
    Added Blast Proof Glass Pane
    
1.5 (Better End):
    Optimized Block model loading (most models now share a common model)
    changed the side texture of glowing obsidian slab
    Bonemeal now generates roses
    Added Overgrown End Stone
    Added Ender Rose
    Added End Grasses
    Added Ender Acid
    Changed world generation of The End
    Added a special surprise that generates in the End!
    Added caves to deep void dimension
    Added Glowing Water
    Added Void Stone Formation
    
1.5.1:
    Added better loot for the creeper, zombie, witch, and blaze
    cave formations can now be replaced with blocks and no longer collide with entities
    The mod can now run on dedicated servers without crashing
    Added Spikes (can be used in mob grinders)

1.5.2:
    Various code optimizations
    Changed the salt lamp to a normal light (the mob repelling tile entity never worked)
    Added lakes of lava and liquid redstone to the deep underground
    Removed lava pockets from the deep underground
    Added lakes of glowing water and lava to the deep void
    Improved world generation in the deep underground and deep void
    Optimized and improved structure generation
    
1.6 (The Comeback):
    Changed Buoys to generate with different colored redstone lamps (including vanilla redstone lamp)
    Reworked Starter House structure to generate in different directions and have biome specific bed colors
    Fixed bug that would cause brick pyramids to generate underground
    Optimized Cave Generators
    Added real trees to the Deep Underground
    Added Blue Ice
    Added Salt Block (the other is now called Compact Salt Block)
    Added Glacier Biome
    Added Ice Shelf biome
    Added Rocky Desert Biome
    Added Salt Flat Biome
    Added Temperate Rainforest Biome
    Added Volcanic Fields biome
    Added Volcanic Mountains biome
    Optimized biome based world generation
    Added a new bonus structure that ChaosDog created!
    Added Research Stations to the Glacier, Salt Flat, and volcanic Biomes
    Added the ability to disable cave pools in dimensions of the player's choice

1.7 (Blue and Gold)
    Optimized the generation of ocean buoys, rogue nether portals, landmines, and small void dungeons (they're hardcoded in the mod now)
    Added fallen trees
    Crystals are now able to be broken faster by pickaxes
    Increased the amount of trees that generate in Temperate Jungle biomes
    Added hills variants of the rocky desert and temperate jungle biomes
    Beautified the Rocky Desert and Volcanic biomes
    Reworked dungeon generation
    Added Golden Redstone Lamp
    Added Squid Meat
    Added jerky meats
    The chunk generation of the deep underground and deep void are finally tied to the world seed!
    
1.7.1:
    Added molten salt lakes to the salt flat biome
    Reworked the decoration of the rocky desert, salt flat, temperate rainforest, and volcanic biomes
    Reworked tree generation in the deep underground
    Reworked lake generation in the end, deep underground, and deep void.
    Added cold and snowy variants of the rocky desert biome

1.7.2:
    Changed mod logo to match the one on my new website!
    
1.8 (The Modularization):
    Cave formations now break if the block under them is removed
    Crystals are now directional and have 4 different render types
    The colored redstone lamps now no longer have 34 classes.
    The door types are now generated from only 1 class
    Overgrown end stone now spreads to end stone
    Fixed world generation bug where foliage in desert and mesa biomes refused to generate in the deep underground and caves
    The Ice Plains Spikes Biome now has much taller spikes
    Rewrote the Deep Underground chunk populator and world generator to eliminate a bunch of bugs with it
    Improved cave world generation (should hopefully be faster)
    Fallen trees now only generate in biomes with trees
    Added prickly pear cactus

1.8.1:
    Fixed bug where crystals in worlds from previous mod versions would be upside down.
