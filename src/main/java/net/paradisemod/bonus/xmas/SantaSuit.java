package net.paradisemod.bonus.xmas;

import net.paradisemod.base.Reference;
import net.paradisemod.bonus.Bonus;
import net.paradisemod.base.ModConfig;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.util.ResourceLocation;

public class SantaSuit extends ItemArmor {
	public SantaSuit(ArmorMaterial materialIn, int renderIndexIn, EntityEquipmentSlot equipmentSlotIn,
			String unlocalizedName) {
		super(materialIn, renderIndexIn, equipmentSlotIn);
		setUnlocalizedName(unlocalizedName);
		setRegistryName(new ResourceLocation(Reference.MOD_ID, unlocalizedName));
		if (!ModConfig.HideXmasFeatures)
			setCreativeTab(Bonus.xmas_tab);
	}
}
