package net.paradisemod.bonus.xmas;

import net.paradisemod.bonus.Bonus;
import net.paradisemod.base.ModConfig;
import net.minecraft.block.BlockGlass;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

import java.util.Random;

public class ChristmasTopper extends BlockGlass {
	public ChristmasTopper() {
		super(Material.PLANTS, false);
		setUnlocalizedName("ChristmasTopper");
		setRegistryName("tree_topper");
		setLightLevel(1F);
		setSoundType(SoundType.METAL);
		if (!ModConfig.HideXmasFeatures)
			setCreativeTab(Bonus.xmas_tab);
	}
	@Override
	protected boolean canSilkHarvest() {
		return false;
	}
	@Override
	public int quantityDropped(Random random) {
		return 1;
	}
}