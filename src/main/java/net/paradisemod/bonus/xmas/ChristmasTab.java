package net.paradisemod.bonus.xmas;

import net.paradisemod.bonus.Bonus;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class ChristmasTab extends CreativeTabs {

public ChristmasTab() {
		super("christmas_stuff");
	}
	@Override
	public ItemStack getTabIconItem() {
		return new ItemStack(Bonus.ChristmasSapling);
	}
}