package net.paradisemod.bonus;

import net.minecraft.block.Block;
import net.minecraft.block.BlockOldLeaf;
import net.minecraft.block.BlockPumpkin;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Reference;
import net.paradisemod.base.Utils;
import net.paradisemod.bonus.structures.EasterEgg;
import net.paradisemod.bonus.structures.GiantGrassBlock;
import net.paradisemod.bonus.structures.PlayerTemples;
import net.paradisemod.bonus.structures.blackCross;
import net.paradisemod.bonus.xmas.*;
import org.apache.logging.log4j.Level;

public class Bonus {
    // christmas stuff
    public static ItemArmor.ArmorMaterial santaSuit = EnumHelper.addArmorMaterial("santa", Reference.MOD_ID + ":santa", -1,new int[] { 0, 0, 0, 0 },0, SoundEvents.ITEM_ARMOR_EQUIP_GENERIC, 0F);
    public static ItemArmor santaJacket = new SantaSuit(santaSuit, 1, EntityEquipmentSlot.CHEST, "santa_jacket");
    public static ItemArmor santaPants = new SantaSuit(santaSuit, 2, EntityEquipmentSlot.LEGS, "santa_pants");
    public static ItemArmor santaBoots = new SantaSuit(santaSuit, 1, EntityEquipmentSlot.FEET, "santa_boots");
    public static Item faceDiaper =  new Item();
    public static CreativeTabs xmas_tab = new ChristmasTab();
    public static net.paradisemod.bonus.xmas.ChristmasSapling ChristmasSapling = new ChristmasSapling();
    public static BlockOldLeaf ChristmasLeaves = new ChristmasLeaves();
    public static Block ChristmasTopper = new ChristmasTopper();
    public static Block Present = new Present();
    public static BlockPumpkin SantaHat = new SantaHat();

    // bonus structures
    public static IWorldGenerator[] structures = {
            new blackCross(),
            new EasterEgg(),
            new PlayerTemples(),
            new GiantGrassBlock(),
    };

    public static void init() {
        Utils.regBlock(ChristmasLeaves);
        Utils.regBlock(ChristmasSapling);
        Utils.regBlock(ChristmasTopper);
        Utils.regBlock(Present);
        Utils.regBlock(SantaHat);
        Utils.regItem(santaJacket);
        Utils.regItem(santaPants);
        Utils.regItem(santaBoots);

        for(IWorldGenerator structure:structures) GameRegistry.registerWorldGenerator(structure, 0);

        zombieCharge.addBannerPattern();

        Utils.regItem(faceDiaper.setUnlocalizedName("faceDiaper").setRegistryName("face_diaper"));

        ParadiseMod.LOG.log(Level.INFO,"Loaded bonus features module");
    }

    public static void regRenders() {
        Utils.regRender(santaJacket);
        Utils.regRender(santaPants);
        Utils.regRender(santaBoots);
        Utils.regRender(ChristmasLeaves);
        Utils.regRender(ChristmasSapling);
        Utils.regRender(ChristmasTopper);
        Utils.regRender(Present);
        Utils.regRender(SantaHat);
        Utils.regRender(faceDiaper);
    }
}
