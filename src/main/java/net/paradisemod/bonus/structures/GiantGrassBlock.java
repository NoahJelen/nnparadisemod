package net.paradisemod.bonus.structures;

import net.paradisemod.base.Reference;
import net.paradisemod.base.ModConfig;
import net.paradisemod.world.modWorld;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Mirror;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.structure.template.PlacementSettings;
import net.minecraft.world.gen.structure.template.Template;
import net.minecraft.world.gen.structure.template.TemplateManager;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class GiantGrassBlock implements IWorldGenerator{
	@Override
	public void generate(Random rand, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator,
						 IChunkProvider chunkProvider) {
		if(ModConfig.worldgen.structures.GiantGrassBlock==false)
			return;
		int blockX = chunkX * 16;
		int blockZ = chunkZ * 16;	
		int y = modWorld.getGroundFromAbove(world, 31, 255, blockX, blockZ, modWorld.ground);
		if (y<=31)
			return;
		WorldServer worldserver = (WorldServer) world;
		MinecraftServer minecraftserver = world.getMinecraftServer();
		TemplateManager templatemanager = worldserver.getStructureTemplateManager();
		Template template = templatemanager.getTemplate(minecraftserver, new ResourceLocation(Reference.MOD_ID+":grass_block_large"));
		BlockPos pos = new BlockPos(blockX, y, blockZ);
		if(rand.nextInt(ModConfig.worldgen.structures.GiantGrassBlockChance) == 0){
			IBlockState iblockstate = world.getBlockState(pos);
			world.notifyBlockUpdate(pos, iblockstate, iblockstate, 3);
			PlacementSettings placementsettings = (new PlacementSettings()).setMirror(Mirror.NONE)
					.setRotation(Rotation.NONE).setIgnoreEntities(false).setChunk((ChunkPos) null)
					.setReplacedBlock((Block) null).setIgnoreStructureBlock(false);
			template.getDataBlocks(pos, placementsettings);
			template.addBlocksToWorld(world, pos, placementsettings);
		}
	}
}