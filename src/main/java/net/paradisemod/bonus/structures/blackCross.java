package net.paradisemod.bonus.structures;

import net.paradisemod.base.Reference;
import net.paradisemod.base.ModConfig;
import net.paradisemod.world.modWorld;
import net.paradisemod.world.dimension.DimensionRegistry;
import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Mirror;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.structure.template.PlacementSettings;
import net.minecraft.world.gen.structure.template.Template;
import net.minecraft.world.gen.structure.template.TemplateManager;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class blackCross implements IWorldGenerator{

	@Override
	public void generate(Random rand, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator,
						 IChunkProvider chunkProvider) {
		if(ModConfig.worldgen.structures.blackCross==false)
			return;
		WorldServer worldserver = (WorldServer) world;
		MinecraftServer minecraftserver = world.getMinecraftServer();
		TemplateManager templatemanager = worldserver.getStructureTemplateManager();
		Template template = templatemanager.getTemplate(minecraftserver, new ResourceLocation(Reference.MOD_ID+":black_cross"));
		int blockX = chunkX * 16;
		int blockZ = chunkZ * 16;
		int y = modWorld.getGroundFromAbove(world, 50, 255, blockX, blockZ, modWorld.ground);
		if (y<=50)
			return;
		boolean isVoid= world.provider.getDimension() == DimensionRegistry.DeepVoid;
		if (isVoid==true)
			template = templatemanager.getTemplate(minecraftserver, new ResourceLocation(Reference.MOD_ID+":black_cross_void"));
		BlockPos pos = new BlockPos(blockX, y, blockZ);
		if(rand.nextInt(777) == 0){
			IBlockState iblockstate = world.getBlockState(pos);
			world.notifyBlockUpdate(pos, iblockstate, iblockstate, 3);
			PlacementSettings placementsettings = (new PlacementSettings()).setMirror(Mirror.NONE)
					.setRotation(Rotation.NONE).setIgnoreEntities(false).setChunk((ChunkPos) null)
					.setReplacedBlock((Block) null).setIgnoreStructureBlock(false);
			template.getDataBlocks(pos, placementsettings);
			template.addBlocksToWorld(world, pos, placementsettings);
			
			//generate supports if needed
			int a=pos.getX();
			int b=pos.getY();
			int c=pos.getZ();
			Block blockToReplace = world.getBlockState(new BlockPos(a,b-1,c)).getBlock();

			//(0,0)
			while ((blockToReplace==Blocks.WATER
					  ||blockToReplace==Blocks.COBBLESTONE
					  ||blockToReplace==Blocks.FLOWING_WATER
					  ||blockToReplace instanceof BlockAir
					  ||blockToReplace instanceof BlockBush
					  ||blockToReplace instanceof BlockSnow
					  ||blockToReplace instanceof BlockFluidClassic
					  ||blockToReplace instanceof BlockLeaves)
					  &&b>0) {
				world.setBlockState(new BlockPos(a,b,c), modWorld.getRandomBrick(rand));
				blockToReplace = world.getBlockState(new BlockPos(a,b-1,c)).getBlock();
				b--;
			}
			b=pos.getY();

			//(7,0)
			blockToReplace = world.getBlockState(new BlockPos(a+6,b-1,c)).getBlock();
			while ((blockToReplace==Blocks.WATER
					  ||blockToReplace==Blocks.COBBLESTONE
					  ||blockToReplace==Blocks.FLOWING_WATER
					  ||blockToReplace instanceof BlockAir
					  ||blockToReplace instanceof BlockBush
					  ||blockToReplace instanceof BlockSnow
					  ||blockToReplace instanceof BlockFluidClassic
					  ||blockToReplace instanceof BlockLeaves)
					  &&b>0) {
				world.setBlockState(new BlockPos(a+6,b,c), modWorld.getRandomBrick(rand));
				blockToReplace = world.getBlockState(new BlockPos(a+6,b-1,c)).getBlock();
				b--;
			}
			b=pos.getY();

			//(0,13)
			blockToReplace = world.getBlockState(new BlockPos(a,b-1,c+12)).getBlock();
			while ((blockToReplace==Blocks.WATER
					  ||blockToReplace==Blocks.COBBLESTONE
					  ||blockToReplace==Blocks.FLOWING_WATER
					  ||blockToReplace instanceof BlockAir
					  ||blockToReplace instanceof BlockBush
					  ||blockToReplace instanceof BlockSnow
					  ||blockToReplace instanceof BlockFluidClassic
					  ||blockToReplace instanceof BlockLeaves)
					  &&b>0) {
				world.setBlockState(new BlockPos(a,b,c+12), modWorld.getRandomBrick(rand));
				blockToReplace = world.getBlockState(new BlockPos(a,b-1,c+12)).getBlock();
				b--;
			}
			b=pos.getY();

			//(7,13)
			blockToReplace = world.getBlockState(new BlockPos(a+6,b-1,c+12)).getBlock();
			while ((blockToReplace==Blocks.WATER
					  ||blockToReplace==Blocks.COBBLESTONE
					  ||blockToReplace==Blocks.FLOWING_WATER
					  ||blockToReplace instanceof BlockAir
					  ||blockToReplace instanceof BlockBush
					  ||blockToReplace instanceof BlockSnow
					  ||blockToReplace instanceof BlockFluidClassic
					  ||blockToReplace instanceof BlockLeaves)
					  &&b>0) {
				world.setBlockState(new BlockPos(a+6,b,c+12), modWorld.getRandomBrick(rand));
				blockToReplace = world.getBlockState(new BlockPos(a+6,b-1,c+12)).getBlock();
				b--;
			}
		}
	}
}