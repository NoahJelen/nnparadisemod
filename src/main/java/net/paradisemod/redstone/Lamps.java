package net.paradisemod.redstone;

import net.paradisemod.base.Utils;
import net.paradisemod.redstone.blocks.CustomRedstoneLamp;
import net.minecraft.block.Block;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class Lamps {
	public static String[] colors = {
			"black",
			"blue",
			"brown",
			"cyan",
			"gold",
			"gray",
			"green",
			"light_blue",
			"lime",
			"magenta",
			"orange",
			"pink",
			"purple",
			"red",
			"silver",
			"white",
			"yellow"
	};
	public static String[] color_names = {
			"Black",
			"Blue",
			"Brown",
			"Cyan",
			"Gold",
			"Gray",
			"Green",
			"LightBlue",
			"Lime",
			"Magenta",
			"Orange",
			"Pink",
			"Purple",
			"Red",
			"Silver",
			"White",
			"Yellow"
	};
	public static Block[] lamps = new Block[17];
	public static Block[] lit_lamps = new Block[17];

	public static void init() {
		for(int i = 0; i < 17; i++) {
			CustomRedstoneLamp lamp = new CustomRedstoneLamp(colors[i],color_names[i],false);
			CustomRedstoneLamp lit_lamp = new CustomRedstoneLamp(colors[i],color_names[i],true);
			lamp.setLitLamp(lit_lamp);
			lit_lamp.setUnlitLamp(lamp);
			lamps[i] = lamp;
			lit_lamps[i] = lit_lamp;
			Utils.regBlock(lamp);
			ForgeRegistries.BLOCKS.register(lit_lamp);
		}
	}

	public static void regRenders() {
		for(Block lamp:lamps) {
			Utils.regRender(lamp);
		}
	}
}