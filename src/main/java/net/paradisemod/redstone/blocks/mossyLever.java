package net.paradisemod.redstone.blocks;

import net.minecraft.block.BlockLever;
import net.minecraft.block.SoundType;
// class for the moss stone lever block
public class mossyLever extends BlockLever {
	public mossyLever() {
		setUnlocalizedName("mossyLever");
		setRegistryName("mossy_lever");
		setSoundType(SoundType.WOOD);
    }
}