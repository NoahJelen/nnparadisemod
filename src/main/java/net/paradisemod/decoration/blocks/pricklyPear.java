package net.paradisemod.decoration.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.modWorld;

import java.util.Arrays;
import java.util.Random;

public class pricklyPear extends Block {
    private static final AxisAlignedBB BB_X = new AxisAlignedBB(4D / 16D,0D,6.5D / 16D,12D / 16D,1D,9.5D / 16D);
    private static final AxisAlignedBB BB_Z = new AxisAlignedBB(6.5D / 16D,0D,4D / 16D,9.5D / 16D,1D,12D / 16D);
    private static final EnumFacing.Axis[] axis_vals = {EnumFacing.Axis.X, EnumFacing.Axis.Z};
    private static final ItemFood FRUIT = Misc.pricklyPearFruit;
    private static final ItemFood NOPAL = Misc.nopal;

    public static final PropertyEnum<EnumFacing.Axis> AXIS = PropertyEnum.create("axis", EnumFacing.Axis.class, axis_vals);
    public static final PropertyEnum<TiltDir> TILT_DIR = PropertyEnum.create("tilt_dir", TiltDir.class);
    public static final PropertyBool HAS_FRUIT = PropertyBool.create("has_fruit");

    public pricklyPear() {
        super(Material.PLANTS);
        setUnlocalizedName("pricklyPear");
        setRegistryName("prickly_pear");
        setDefaultState(this.blockState.getBaseState().withProperty(TILT_DIR, TiltDir.NONE).withProperty(AXIS, EnumFacing.Axis.X).withProperty(HAS_FRUIT, false));
        setSoundType(SoundType.CLOTH);
        setCreativeTab(CreativeTabs.DECORATIONS);
        setTickRandomly(true);
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand) {
        int blockY = pos.getY();
        int groundY = modWorld.getGroundFromAbove(world,0,blockY + 3,pos.getX(),pos.getZ());
        if (blockY - groundY >= 5) return;
        BlockPos[] spreadTo = {
                pos.up().east(),
                pos.up().west(),
                pos.up()
        };
        if (state.getValue(AXIS) == EnumFacing.Axis.Z) {
            spreadTo[0] = pos.up().north();
            spreadTo[1] = pos.up().south();
        }
        BlockPos newpos = spreadTo[rand.nextInt(3)];
        if (world.getBlockState(newpos).getBlock() != Blocks.AIR) return;
        if (world.getBlockState(newpos).getBlock() == this) return;
        world.setBlockState(newpos,state.withProperty(HAS_FRUIT, rand.nextBoolean()));
        neighborChanged(state,world,pos,this, pos);
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, TILT_DIR, AXIS, HAS_FRUIT);
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        switch (state.getValue(AXIS)) {
            case X:
                return BB_X;

            case Z:
                return BB_Z;
        }

        return BB_X;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess world, BlockPos pos) {
        return getBoundingBox(blockState,world,pos);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        switch (meta) {
            case 0:
                return getDefaultState().withProperty(HAS_FRUIT, false).withProperty(AXIS, EnumFacing.Axis.X);
            case 1:
                return getDefaultState().withProperty(HAS_FRUIT, true).withProperty(AXIS, EnumFacing.Axis.X);
            case 2:
                return getDefaultState().withProperty(HAS_FRUIT, false).withProperty(AXIS, EnumFacing.Axis.Z);
            case 3:
                return getDefaultState().withProperty(HAS_FRUIT, true).withProperty(AXIS, EnumFacing.Axis.Z);
        }
        return getDefaultState();
    }

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess world, BlockPos pos) {
        if ((world.getBlockState(pos.east()).getBlock() == this && world.getBlockState(pos.west()).getBlock() == this) || (world.getBlockState(pos.north()).getBlock() == this && world.getBlockState(pos.south()).getBlock() == this))
            return state.withProperty(TILT_DIR, TiltDir.NONE);
        else if (world.getBlockState(pos.down().east()).getBlock() == this)
            return state.withProperty(TILT_DIR,TiltDir.WEST);
        else if (world.getBlockState(pos.down().west()).getBlock() == this)
            return state.withProperty(TILT_DIR,TiltDir.EAST);
        else if (world.getBlockState(pos.down().north()).getBlock() == this)
            return state.withProperty(TILT_DIR,TiltDir.SOUTH);
        else if (world.getBlockState(pos.down().south()).getBlock() == this)
            return state.withProperty(TILT_DIR,TiltDir.NORTH);
        return state;
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        boolean has_fruit = state.getValue(HAS_FRUIT);
        EnumFacing.Axis axis = state.getValue(AXIS);

        if (!has_fruit && axis == EnumFacing.Axis.X)
            return 0;
        else if (has_fruit && axis == EnumFacing.Axis.X)
            return 1;
        else if (!has_fruit && axis == EnumFacing.Axis.Z)
            return 2;
        else if (has_fruit && axis == EnumFacing.Axis.Z)
            return 3;
        return 0;
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block block, BlockPos fromPos) {
        if (canPlaceBlockOnSide(world, pos, EnumFacing.UP)) return;
        dropBlockAsItemWithChance(world, pos, state, 1.0f, 0);
        world.setBlockState(pos, Blocks.AIR.getDefaultState());
    }

    @Override
    public boolean canPlaceBlockOnSide(World world, BlockPos pos, EnumFacing side) {
        Block[] sideBlocks = {
                world.getBlockState(pos.down().east()).getBlock(),
                world.getBlockState(pos.down().west()).getBlock(),
                world.getBlockState(pos.down().north()).getBlock(),
                world.getBlockState(pos.down().south()).getBlock(),
        };
        Block blockBelow = world.getBlockState(pos.down()).getBlock();

        for (Block sideBlock: sideBlocks)
            if (sideBlock == this) return true;
        if (Arrays.asList(modWorld.ground).contains(blockBelow) || blockBelow == this) return true;
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) { return false; }

    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
        switch (facing.getAxis()) {
            case X:
                return getDefaultState().withProperty(AXIS, EnumFacing.Axis.Z);
            case Y: case Z:
                return getDefaultState().withProperty(AXIS, EnumFacing.Axis.X);
        }
        return getDefaultState().withProperty(AXIS, EnumFacing.Axis.X);
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        if (state.getValue(HAS_FRUIT)) return FRUIT;
        return NOPAL;
    }

    @Override
    public void onEntityCollidedWithBlock(World world, BlockPos pos, IBlockState state, Entity entity) {
        // is the entity a creature?
        if (!(entity instanceof EntityLivingBase)) return;

        EntityLivingBase creature= (EntityLivingBase) entity;
        creature.attackEntityFrom(DamageSource.CACTUS, 2F);
    }

    public enum TiltDir implements IStringSerializable {
        EAST("east"),
        WEST("west"),
        NORTH("north"),
        SOUTH("south"),
        NONE("none");
        private final String name;

        TiltDir(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }
    }
}

