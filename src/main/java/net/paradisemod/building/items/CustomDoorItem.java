package net.paradisemod.building.items;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemDoor;

public class CustomDoorItem extends ItemDoor {
    public CustomDoorItem(Block block, String regName, String ULName) {
        super(block);
        setRegistryName(regName);
        setUnlocalizedName(ULName);
        setCreativeTab(CreativeTabs.REDSTONE);
    }
}
