package net.paradisemod.building.blocks;

import net.minecraft.block.BlockDoor;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

import java.util.Random;

public class CustomDoor extends BlockDoor {
    private Item doorItem = Items.OAK_DOOR;
    public CustomDoor(Material material, String regName, Float hardness, Float res, String tool, int hlevel, SoundType sound) {
        super(material);
        setRegistryName(regName);
        setHardness(hardness);
        setResistance(res);
        setHarvestLevel(tool,hlevel);
        setSoundType(sound);
    }

    // this method should be called after registering associated door item
    public void setItem(Item item) {
        doorItem = item;
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return state.getValue(HALF) == BlockDoor.EnumDoorHalf.UPPER ? Items.AIR : doorItem;
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
    {
        return new ItemStack(doorItem);
    }
}
