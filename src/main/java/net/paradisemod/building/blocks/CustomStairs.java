package net.paradisemod.building.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;
import net.minecraft.creativetab.CreativeTabs;
//base class for stair blocks
public class CustomStairs extends BlockStairs {
	public CustomStairs(Block stairType) {
		super(stairType.getDefaultState());
		setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		useNeighborBrightness = true;
	}
}