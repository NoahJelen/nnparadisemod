package net.paradisemod.building;

import net.paradisemod.building.blocks.CustomDoor;
import net.paradisemod.building.blocks.RedstoneDoor;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class Doors {
	// doors
	public static CustomDoor CactusDoor;
	public static CustomDoor EndDoor;
	public static CustomDoor SilverDoor;
	public static CustomDoor GlassDoor;
	public static CustomDoor GoldDoor;
	public static CustomDoor DiamondDoor;
	public static CustomDoor EmeraldDoor;
	public static CustomDoor AndesiteDoor;
	public static CustomDoor DioriteDoor;
	public static CustomDoor GraniteDoor;
	public static CustomDoor StoneDoor;
	public static CustomDoor CobblestoneDoor;
	public static CustomDoor MossStoneDoor;
	public static CustomDoor GlowingObsidianDoor;
	public static CustomDoor ObsidianDoor;
	public static CustomDoor BedrockDoor;
	public static CustomDoor RubyDoor;
	public static CustomDoor RustyDoor;
	public static Block RedstoneDoor = new RedstoneDoor();

	public static void init() {
		// door blocks
		// they don't need their own items
		ForgeRegistries.BLOCKS.register(AndesiteDoor = new CustomDoor(Material.IRON,"andesite_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(BedrockDoor = new CustomDoor(Material.IRON,"bedrock_door_block",-1f,6000000F,"pickaxe",6, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(CactusDoor = new CustomDoor(Material.WOOD,"cactus_door_block",5f,10F,"axe",0,SoundType.WOOD));
		ForgeRegistries.BLOCKS.register(CobblestoneDoor = new CustomDoor(Material.IRON,"cobblestone_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(DiamondDoor = new CustomDoor(Material.IRON,"diamond_door_block",5f,10F,"pickaxe",2, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(DioriteDoor = new CustomDoor(Material.IRON,"diorite_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(EmeraldDoor = new CustomDoor(Material.WOOD,"emerald_door_block",5f,10F,"pickaxe",2, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(EndDoor = new CustomDoor(Material.IRON,"end_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(GlassDoor = new CustomDoor(Material.WOOD,"glass_door_block",5f,10F,"pickaxe",0, SoundType.GLASS));
		ForgeRegistries.BLOCKS.register(GlowingObsidianDoor = new CustomDoor(Material.IRON,"glowing_obsidian_door_block",5f,10F,"pickaxe",3, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(GoldDoor = new CustomDoor(Material.IRON,"gold_door_block",5f,10F,"pickaxe",2, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(GraniteDoor = new CustomDoor(Material.IRON,"granite_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(MossStoneDoor = new CustomDoor(Material.IRON,"moss_stone_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(ObsidianDoor = new CustomDoor(Material.IRON,"obsidian_door_block",5f,10F,"pickaxe",3, SoundType.STONE));
		ForgeRegistries.BLOCKS.register(RedstoneDoor);
		ForgeRegistries.BLOCKS.register(RubyDoor = new CustomDoor(Material.WOOD,"ruby_door_block",5f,10F,"pickaxe",2, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(RustyDoor = new CustomDoor(Material.IRON,"rusty_door_block",5f,10F,"pickaxe",1, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(SilverDoor = new CustomDoor(Material.IRON,"silver_door_block",5f,10F,"pickaxe",2, SoundType.METAL));
		ForgeRegistries.BLOCKS.register(StoneDoor = new CustomDoor(Material.IRON,"stone_door_block",5f,10F,"pickaxe",0, SoundType.STONE));
	}
}
