package net.paradisemod.misc.items.tools.emerald;

import net.paradisemod.base.Reference;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.util.ResourceLocation;

public class emeraldPickaxe extends ItemPickaxe {
	public emeraldPickaxe(ToolMaterial material, String unlocalizedName) {
		super(material);
		setUnlocalizedName(unlocalizedName);
		setRegistryName(new ResourceLocation(Reference.MOD_ID, unlocalizedName));
	}
}