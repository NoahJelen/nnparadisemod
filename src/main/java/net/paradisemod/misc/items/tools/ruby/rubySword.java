package net.paradisemod.misc.items.tools.ruby;

import net.paradisemod.base.Reference;

import net.minecraft.item.ItemSword;
import net.minecraft.util.ResourceLocation;

public class rubySword extends ItemSword {
	public rubySword(ToolMaterial material, String unlocalizedName) {
		super(material);
		setUnlocalizedName(unlocalizedName);
		setRegistryName(new ResourceLocation(Reference.MOD_ID, unlocalizedName));
	}
}