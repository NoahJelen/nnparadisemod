package net.paradisemod.misc;

import net.paradisemod.world.Ores;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class Smelting {
    public static void init() {
        //cooked egg recipe
        GameRegistry.addSmelting(Items.EGG, new ItemStack(Misc.CookedEgg), .01F);

        //cooked squid meat
        GameRegistry.addSmelting(Misc.Calamari, new ItemStack(Misc.cookedCalamari), .01F);

        //rubies and silver ingots can obtained by smelting their ores
        GameRegistry.addSmelting(Ores.RubyOre, new ItemStack(Misc.Ruby), 5F);
        GameRegistry.addSmelting(Ores.SilverOre, new ItemStack(Misc.SilverIngot), 5F);

        //smelting recipes for end ores
        GameRegistry.addSmelting(Ores.EndRubyOre, new ItemStack(Misc.Ruby), 5F);
        GameRegistry.addSmelting(Ores.EnderPearlOre, new ItemStack(Items.ENDER_PEARL), 5F);
        GameRegistry.addSmelting(Ores.VoidPearlOre, new ItemStack(Misc.VoidPearl), 5F);

        //smelting recipes for nether ores
        GameRegistry.addSmelting(Ores.SilverOreNether, new ItemStack(Misc.SilverIngot), 5F);
        GameRegistry.addSmelting(Ores.GoldOreNether, new ItemStack(Items.GOLD_INGOT), 5F);

        //smelting recipes for void ores
        GameRegistry.addSmelting(Ores.GoldOreVoid, new ItemStack(Items.GOLD_INGOT), 5F);
        GameRegistry.addSmelting(Ores.IronOreVoid, new ItemStack(Items.IRON_INGOT), 5F);
        GameRegistry.addSmelting(Ores.CoalOreVoid, new ItemStack(Items.COAL), 5F);
        GameRegistry.addSmelting(Ores.SilverOreVoid, new ItemStack(Misc.SilverIngot), 5F);

        //silver nuggets can be obtained by smelting stuff made of silver
        GameRegistry.addSmelting(Armor.silverHelmet, new ItemStack(Misc.SilverNugget),5F);
        GameRegistry.addSmelting(Armor.silverChestplate,new ItemStack(Misc.SilverNugget),5F);
        GameRegistry.addSmelting(Armor.silverLeggings,new ItemStack(Misc.SilverNugget),5F);
        GameRegistry.addSmelting(Armor.silverBoots,new ItemStack(Misc.SilverNugget),5F);
        GameRegistry.addSmelting(Tools.silverAxe, new ItemStack(Misc.SilverNugget),5F);
        GameRegistry.addSmelting(Tools.silverSpade, new ItemStack(Misc.SilverNugget),5F);
        GameRegistry.addSmelting(Tools.silverPickaxe, new ItemStack(Misc.SilverNugget),5F);
        GameRegistry.addSmelting(Tools.silverHoe, new ItemStack(Misc.SilverNugget),5F);
    }
}
