package net.paradisemod.misc.blocks;

import net.minecraft.block.BlockAir;
// class for the underground air block
// this block is used internally in the mod for structures
public class undergroundAir extends BlockAir {
	public undergroundAir() {
		setUnlocalizedName("undergroundAir");
		setRegistryName("underground_air");
	}
}