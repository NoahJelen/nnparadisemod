package net.paradisemod.misc;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockBreakable;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemEnderPearl;
import net.minecraft.item.ItemFood;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Utils;
import net.paradisemod.decoration.blocks.CustomGlass;
import net.paradisemod.misc.blocks.*;
import net.paradisemod.misc.tileentity.TileEntityCactusChest;
import net.paradisemod.misc.tileentity.TileEntityCompressedCactusChest;
import org.apache.logging.log4j.Level;

public class Misc {
    // food
    public static ItemFood CookedEgg = new ItemFood(8, true);
    public static ItemFood BeefJerky = new ItemFood(5, true);
    public static ItemFood ChickenJerky = new ItemFood(4, true);
    public static ItemFood MuttonJerky = new ItemFood(4, true);
    public static ItemFood PorkJerky = new ItemFood(5, true);
    public static ItemFood Calamari = new ItemFood(2, true);
    public static ItemFood cookedCalamari = new ItemFood(6, true);
    public static ItemFood nopal = new ItemFood(1, false);
    public static ItemFood pricklyPearFruit = new ItemFood(4, false);

    // dyes
    public static Item BlackDye = new Item();
    public static Item BlueDye = new Item();
    public static Item BrownDye = new Item();

    // basic material items
    // this is how a basic item can be added
    public static Item CactusStick = new Item();
    public static Item Ruby = new Item();
    public static Item RustyIngot = new Item();
    public static Item RustyNugget = new Item();
    public static Item salt = new Item();
    public static Item SilverIngot = new Item();
    public static Item SilverNugget = new Item();
    public static Item VoidPearl=new ItemEnderPearl();

    // chests
    public static Block CactusChest = new CactusChest();
    public static Block CactusChestTrapped = new CactusChestTrapped();
    public static Block CompressedCactusChest = new CompressedCactusChest();
    public static Block CompressedCactusChestTrapped = new CompressedCactusChestTrapped();

    // other blocks
    public static CustomGlass glowingIce = new CustomGlass("glowing_ice","glowingIce");
    public static CustomGlass blueIce = new CustomGlass("blue_ice","blueIce");
    public static BlockBreakable prismarineCrystalBlock = new prismarineCrystalBlock();
    public static Block SaltLamp;
    public static BlockAir undergroundAir = new undergroundAir();

    public static void init() {
        // food
        Utils.regItem(CookedEgg.setUnlocalizedName("EggCooked").setRegistryName("cooked_egg"));
        Utils.regItem(BeefJerky.setUnlocalizedName("BeefJerky").setRegistryName("beef_jerky"));
        Utils.regItem(ChickenJerky.setUnlocalizedName("ChickenJerky").setRegistryName("chicken_jerky"));
        Utils.regItem(MuttonJerky.setUnlocalizedName("MuttonJerky").setRegistryName("mutton_jerky"));
        Utils.regItem(PorkJerky.setUnlocalizedName("PorkJerky").setRegistryName("pork_jerky"));
        Utils.regItem(Calamari.setUnlocalizedName("Calamari").setRegistryName("calamari"));
        Utils.regItem(cookedCalamari.setUnlocalizedName("cookedCalamari").setRegistryName("cooked_calamari"));
        Utils.regItem(nopal.setUnlocalizedName("nopal").setRegistryName("nopal"));
        Utils.regItem(pricklyPearFruit.setUnlocalizedName("pricklyPearFruit").setRegistryName("prickly_pear_fruit"));

        // dyes
        // this is how the properties of an item object are set
        Utils.regItem(BlackDye.setUnlocalizedName("DyeBlack").setRegistryName("black_dye").setCreativeTab(CreativeTabs.MISC));
        Utils.regItem(BlueDye.setUnlocalizedName("DyeBlue").setRegistryName("blue_dye").setCreativeTab(CreativeTabs.MISC));
        Utils.regItem(BrownDye.setUnlocalizedName("DyeBrown").setRegistryName("brown_dye").setCreativeTab(CreativeTabs.MISC));

        // basic material items
        Utils.regItem(CactusStick.setUnlocalizedName("CactusStick").setRegistryName("cactus_stick").setCreativeTab(CreativeTabs.MISC));
        Utils.regItem(Ruby.setUnlocalizedName("Ruby").setRegistryName("ruby").setCreativeTab(CreativeTabs.MISC));
        Utils.regItem(RustyIngot.setUnlocalizedName("RustyIngot").setRegistryName("rusty_ingot").setCreativeTab(CreativeTabs.MISC));
        Utils.regItem(RustyNugget.setUnlocalizedName("RustyNugget").setRegistryName("rusty_nugget").setCreativeTab(CreativeTabs.MISC));
        Utils.regItem(salt.setUnlocalizedName("salt").setRegistryName("salt").setCreativeTab(CreativeTabs.MISC));
        Utils.regItem(SilverIngot.setUnlocalizedName("SilverIngot").setRegistryName("silver_ingot").setCreativeTab(CreativeTabs.MISC));
        Utils.regItem(SilverNugget.setUnlocalizedName("SilverNugget").setRegistryName("silver_nugget").setCreativeTab(CreativeTabs.MISC));
        Utils.regItem(VoidPearl.setUnlocalizedName("VoidPearl").setRegistryName("void_pearl").setCreativeTab(CreativeTabs.MISC));

        // chests
        Utils.regBlock(CactusChest);
        Utils.regBlock(CactusChestTrapped);
        Utils.regBlock(CompressedCactusChest);
        Utils.regBlock(CompressedCactusChestTrapped);

        // chest tile entities
        GameRegistry.registerTileEntity(TileEntityCactusChest.class,"cactus_chest");
        GameRegistry.registerTileEntity(TileEntityCompressedCactusChest.class,"compressed_cactus_chest");

        // other blocks
        glowingIce.setDefaultSlipperiness(1F);
        blueIce.setDefaultSlipperiness(1F);

        Utils.regBlock(glowingIce.setHardness(.2F).setResistance(2F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS).setLightLevel(.46666667F));
        Utils.regBlock(blueIce.setHardness(.2F).setResistance(2F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS));
        Utils.regBlock(prismarineCrystalBlock);
        Utils.regBlock(SaltLamp = new SaltLamp());
        ForgeRegistries.BLOCKS.register(undergroundAir);

        // smelting recipes
        Smelting.init();

        // tools
        Tools.init();

        // amror
        Armor.init();

        ParadiseMod.LOG.log(Level.INFO,"Loaded miscellaneous module");
    }

    public static void regRenders() {
        Utils.regRender(CookedEgg);
        Utils.regRender(BeefJerky);
        Utils.regRender(ChickenJerky);
        Utils.regRender(MuttonJerky);
        Utils.regRender(PorkJerky);
        Utils.regRender(Calamari);
        Utils.regRender(cookedCalamari);
        Utils.regRender(BlackDye);
        Utils.regRender(BlueDye);
        Utils.regRender(BrownDye);
        Utils.regRender(CactusStick);
        Utils.regRender(Ruby);
        Utils.regRender(RustyIngot);
        Utils.regRender(RustyNugget);
        Utils.regRender(salt);
        Utils.regRender(SilverIngot);
        Utils.regRender(SilverNugget);
        Utils.regRender(VoidPearl);
        Utils.regRender(glowingIce);
        Utils.regRender(blueIce);
        Utils.regRender(prismarineCrystalBlock);
        Utils.regRender(SaltLamp);
        Utils.regRender(CactusChest);
        Utils.regRender(CactusChestTrapped);
        Utils.regRender(CompressedCactusChest);
        Utils.regRender(CompressedCactusChestTrapped);
        Utils.regRender(nopal);
        Utils.regRender(pricklyPearFruit);

        Tools.regRenders();
        Armor.regRenders();
    }
}
