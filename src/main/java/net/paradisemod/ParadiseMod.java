package net.paradisemod;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.paradisemod.automation.Automation;
import net.paradisemod.base.Events;
import net.paradisemod.base.OreDictHandler;
import net.paradisemod.base.Reference;
import net.paradisemod.base.proxy.CommonProxy;
import net.paradisemod.bonus.Bonus;
import net.paradisemod.building.Building;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.misc.Misc;
import net.paradisemod.redstone.Redstone;
import net.paradisemod.world.modWorld;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = Reference.MOD_ID, name = Reference.NAME, version = Reference.VERSION, acceptedMinecraftVersions = Reference.ACCEPTED_VERSIONS)
public class ParadiseMod {
	static {
		FluidRegistry.enableUniversalBucket();
	}

	public static final Logger LOG = LogManager.getLogger("Paradise Mod");
	@Instance
	public static ParadiseMod instance;

	@SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
	public static CommonProxy proxy;

	@SuppressWarnings("deprecation")
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// loot tables
		LootTableList.register(new ResourceLocation("nnparadisemod:starter_chest"));
		LootTableList.register(new ResourceLocation("nnparadisemod:mining_chest"));
		LootTableList.register(new ResourceLocation("nnparadisemod:treasure_chest"));
        
        // events
        MinecraftForge.EVENT_BUS.register(Events.class);

        // some modifications to vanilla blocks
        // This is NOT a coremod!
		Blocks.REDSTONE_BLOCK.setResistance(2000F);
		Blocks.WOODEN_BUTTON.setUnlocalizedName("oak_button");
		Blocks.STRUCTURE_BLOCK.setCreativeTab(CreativeTabs.REDSTONE);
		Blocks.STRUCTURE_VOID.setCreativeTab(CreativeTabs.REDSTONE);
		Blocks.COMMAND_BLOCK.setCreativeTab(CreativeTabs.REDSTONE);
		Blocks.REPEATING_COMMAND_BLOCK.setCreativeTab(CreativeTabs.REDSTONE);
		Blocks.CHAIN_COMMAND_BLOCK.setCreativeTab(CreativeTabs.REDSTONE);
		Blocks.BARRIER.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		Blocks.MOB_SPAWNER.setCreativeTab(CreativeTabs.DECORATIONS);

		// modules
		Automation.init();
		Bonus.init();
		Building.init();
		Decoration.init();
		Misc.init();
		Redstone.init();
		modWorld.init();
	}
	@EventHandler
	public void Init(FMLInitializationEvent event) {
		OreDictHandler.registerOreDict();
		proxy.init();
		Decoration.initTableGUI();
	}
	@EventHandler
	public void PostInit(FMLPostInitializationEvent event) {
		if (proxy.isClient()) {
			LOG.log(Level.INFO,"Minecraft is in client mode");
		}
		else {
			LOG.log(Level.INFO,"Minecraft is in server mode");
		}
	}
}