package net.paradisemod.world;

import net.minecraftforge.fml.common.IWorldGenerator;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.paradisemod.world.biome.gen.GlacierGen;
import net.paradisemod.world.gen.betterEnd.EndGrass;
import net.paradisemod.world.gen.betterEnd.EndSurprise;
import net.paradisemod.world.gen.caveGen.*;
import net.paradisemod.world.gen.misc.*;
import net.paradisemod.world.gen.ores.OreGenEnd;
import net.paradisemod.world.gen.ores.OreGenNether;
import net.paradisemod.world.gen.ores.OreGenOverworld;
import net.paradisemod.world.gen.structures.*;

public class worldGen {
    public static void init() {
        // ores
        IWorldGenerator[] ores= {
                // overworld ore gen
                new OreGenOverworld(),

                // nether ore gen
                new OreGenNether(),

                // end ore gen
                new OreGenEnd()
        };

        // structures
        IWorldGenerator[] structures= {
                new Home(),
                new ResearchBase(),
                new WickerMan(),
                new Ocean(),
                new Dungeon(),
                new RoguePortal(),
                new Runway(),
                new TreasureChest(),
                new SkyWheel(),
                new LandMine(),
                new Buoy(),
                new TreasureChest(),
                new SkyWheel(),
                new UndergroundVillage()
        };

        // miscellaneous
        IWorldGenerator[] misc= {
                // dirt, gravel, sand, and clay on the ocean floors
                new SeaFloorGen(),

                // flowers
                new Rose(),
                new EndSurprise(),

                // fallen trees (similar to the MCBE feature)
                new FallenTree()
        };

        // cave gen
        GameRegistry.registerWorldGenerator(new CaveGenBase(),0);
        GameRegistry.registerWorldGenerator(new CaveGenCrystal(),0);

        // prickly pear cactus
        GameRegistry.registerWorldGenerator(new pricklyPearGen(), 0);

        for(IWorldGenerator generator:ores) GameRegistry.registerWorldGenerator(generator,3);
        for(IWorldGenerator generator:misc) GameRegistry.registerWorldGenerator(generator,0);
        for(IWorldGenerator generator:structures)  GameRegistry.registerWorldGenerator(generator,0);
        GameRegistry.registerWorldGenerator(new GlacierGen(),0);
        GameRegistry.registerWorldGenerator(new EndGrass(),0);

        // lakes
        GameRegistry.registerWorldGenerator(new CustomLakes(),0);

        // new ice spike
        GameRegistry.registerWorldGenerator(new newIceSpike(),100);
    }
}
