package net.paradisemod.world;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.BonemealEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.paradisemod.decoration.Decoration;

import java.util.Random;

public class Events {
    @SubscribeEvent(priority= EventPriority.LOWEST)
    public static void growEndPlants(BonemealEvent event) {
        World world = event.getWorld();
        Random rand = world.rand;
        BlockPos pos = event.getPos();
        IBlockState state = world.getBlockState(pos);
        Block[] plants = {Decoration.EnderRose, modWorld.EndGrass,modWorld.TallEndGrass};
        if (state==modWorld.EndGrass.getDefaultState()) {
            world.setBlockState(new BlockPos(pos.getX(),pos.getY(),pos.getZ()), modWorld.TallEndGrass.getDefaultState());
            return;
        }

        if (state!=modWorld.OvergrownEndStone.getDefaultState())
            return;

        if (world.isRemote)
            return;

        int x = pos.getX()-7;
        int y = pos.getY()-7;
        int z = pos.getZ()-7;
        world.setBlockState(new BlockPos(pos.getX(),pos.getY()+1,pos.getZ()), plants[rand.nextInt(3)].getDefaultState());
        if (y<0)
            y=0;
        for (int bx = x; bx < x+14; bx++) {
            for (int bz = z; bz < z+14; bz++) {
                for (int by = y; by < y+14; by++) {
                    if (world.getBlockState(new BlockPos(bx,by,bz))==modWorld.OvergrownEndStone.getDefaultState() &&
                            world.getBlockState(new BlockPos(bx,by+1,bz))== Blocks.AIR.getDefaultState() &&
                            rand.nextInt(15)==0)
                        world.setBlockState(new BlockPos(bx,by+1,bz), plants[rand.nextInt(3)].getDefaultState());
                }
            }
        }
    }

    @SubscribeEvent(priority=EventPriority.LOWEST)
    public static void growRoses(BonemealEvent event) {
        World world = event.getWorld();
        Random rand = world.rand;
        BlockPos pos = event.getPos();
        IBlockState state = world.getBlockState(pos);

        if (state!=Blocks.GRASS.getDefaultState())
            return;
        if (world.isRemote)
            return;
        int x = pos.getX()-7;
        int y = pos.getY()-7;
        int z = pos.getZ()-7;

        if (y<0)
            y=0;
        Block[] roses = {Decoration.BlueRose,Decoration.Rose};
        for (int bx = x; bx < x+14; bx++) {
            for (int bz = z; bz < z+14; bz++) {
                for (int by = y; by < y+14; by++) {
                    if (world.getBlockState(new BlockPos(bx,by,bz))==Blocks.GRASS.getDefaultState() &&
                            world.getBlockState(new BlockPos(bx,by+1,bz))==Blocks.AIR.getDefaultState() &&
                            rand.nextInt(16)==0)
                        world.setBlockState(new BlockPos(bx,by+1,bz), roses[rand.nextInt(2)].getDefaultState());
                }
            }
        }
    }
}