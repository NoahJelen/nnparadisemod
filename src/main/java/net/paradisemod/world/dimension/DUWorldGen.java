package net.paradisemod.world.dimension;

import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.*;
import net.minecraft.world.chunk.Chunk;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.Ores;
import net.paradisemod.world.biome.BiomeRegistry;
import net.paradisemod.world.biome.BiomeRockyDesert;
import net.paradisemod.world.biome.BiomeTemperateJungle;
import net.paradisemod.world.gen.misc.LargePlants;
import net.paradisemod.world.modWorld;

import java.util.Random;

// world generation for the deep underground dimension
public class DUWorldGen {
    private static final IBlockState[] STONES = {
            Blocks.STONE.getDefaultState().withProperty(BlockStone.VARIANT,BlockStone.EnumType.ANDESITE), // andesite
            Blocks.STONE.getDefaultState().withProperty(BlockStone.VARIANT,BlockStone.EnumType.DIORITE), // diorite
            Blocks.STONE.getDefaultState().withProperty(BlockStone.VARIANT,BlockStone.EnumType.GRANITE) // granite
    };

    private static final IBlockState[] ORES = {
            Blocks.COAL_ORE.getDefaultState(),
            Blocks.IRON_ORE.getDefaultState(),
            Blocks.GOLD_ORE.getDefaultState(),
            Ores.SilverOre.getDefaultState()
    };

    private static final int[] chances = {28, 14, 7, 7};

    private static final IBlockState GLOWSTONE = Blocks.GLOWSTONE.getDefaultState();
    private static final IBlockState SEA_LANTERN = Blocks.SEA_LANTERN.getDefaultState();

    public static void generate(Random rand, int chunkX, int chunkZ, World world) {
        BlockPos pos = new BlockPos(chunkX * 16, rand.nextInt(127), chunkZ * 16);
        Chunk chunk = world.getChunkFromChunkCoords(chunkX, chunkZ);

        genStones(rand, world, pos);
        genOres(rand, world, pos);
        genGlowStone(rand, world, pos);
        genBigPlants(rand, world, chunkX, chunkZ);
        genFoliage(rand,chunk);
        genSnow(world, chunk);

        chunk.checkLight();
    }

    private static void genOres(Random rand, World world, BlockPos pos) {
        int val = rand.nextInt(4);
        modWorld.generateOre(ORES[val],world,rand,pos.getX(),pos.getZ(),0,127,10,chances[val], Blocks.STONE);
    }

    private static void genBigPlants(Random rand, World world, int chunkX, int chunkZ) {
        int[] heights = {127, 80, 60};
        for (int i = 0; i < 13; i++) {
            int blockX = (chunkX * 16) + 3 + rand.nextInt(10);
            int blockZ = (chunkZ * 16) + 3 + rand.nextInt(10);
            int y = modWorld.getGroundFromAbove(world, 31, heights[rand.nextInt(3)], blockX, blockZ, Blocks.GRASS);
            BlockPos position = new BlockPos(blockX, y + 1, blockZ);
            if (!hasSpaceToSpawn(position, world)) continue;
            if (y > 31) {
                Biome biome = world.getBiomeForCoordsBody(position);
                // temperate rainforest trees
                if (biome instanceof BiomeTemperateJungle){
                    if (rand.nextBoolean())
                        LargePlants.genSpruceTree(world, rand, position);
                    else
                        LargePlants.genClassicTree(world, rand, position,1 + rand.nextInt(2),true);
                }

                // oak or birch tree (forest biomes)
                if (biome== Biomes.FOREST || biome == Biomes.FOREST_HILLS || biome == Biomes.MUTATED_FOREST) {
                    if (rand.nextInt(10) == 0)
                        LargePlants.genClassicTree(world, rand, position,1,false);
                    else
                        LargePlants.genClassicTree(world, rand, position,0,false);
                }

                // swamp oak tree
                if (biome instanceof BiomeSwamp)
                    LargePlants.genClassicTree(world, rand, position,0,true);

                // birch tree
                if(biome == Biomes.BIRCH_FOREST||biome == Biomes.BIRCH_FOREST_HILLS||biome == Biomes.MUTATED_BIRCH_FOREST||biome == Biomes.MUTATED_BIRCH_FOREST_HILLS)
                    LargePlants.genClassicTree(world, rand, position,1,false);

                // jungle tree
                if(biome instanceof BiomeJungle) {
                    LargePlants.genClassicTree(world, rand, position,2,true);
                    // generate some bushes
                    for (int j = 0; j < 5; j++) {
                        blockX = (chunkX * 16)+ 3 + rand.nextInt(10);
                        blockZ = (chunkZ * 16)+ 3 + rand.nextInt(10);
                        y = modWorld.getGroundFromAbove(world, 31, heights[rand.nextInt(3)], blockX, blockZ, Blocks.GRASS);
                        position = new BlockPos(blockX, y+1, blockZ);
                        if (y>30)
                            LargePlants.genJungleBush(world, rand, position);
                    }
                }

                // spruce tree
                if(biome instanceof BiomeTaiga)
                    LargePlants.genSpruceTree(world, rand, position);

                // dark oak tree
                if(biome == Biomes.ROOFED_FOREST || biome == Biomes.MUTATED_ROOFED_FOREST)
                    LargePlants.genBigOakTree(world, position, rand);

                // acacia tree
                if(biome instanceof BiomeSavanna)
                    LargePlants.genAcaciaTree(world, rand, position);

                // cactus
                if(biome instanceof BiomeDesert || biome instanceof BiomeRockyDesert) {
                    blockX = (chunkX * 16)+ rand.nextInt(16);
                    blockZ = (chunkZ * 16)+ rand.nextInt(16);
                    y = modWorld.getGroundFromAbove(world, 31, heights[rand.nextInt(3)], blockX, blockZ, new Block[] {Blocks.SAND, Blocks.SANDSTONE});
                    position = new BlockPos(blockX, y, blockZ);
                    LargePlants.genCactus(world, position, rand, 0);
                }

                if (biome == Biomes.MESA || biome == Biomes.MESA_CLEAR_ROCK || biome == Biomes.MUTATED_MESA || biome == Biomes.MUTATED_MESA_CLEAR_ROCK || biome == Biomes.MUTATED_MESA_ROCK) {
                    blockX = (chunkX * 16) + rand.nextInt(16);
                    blockZ = (chunkZ * 16) + rand.nextInt(16);
                    y = modWorld.getGroundFromAbove(world, 31, heights[rand.nextInt(3)], blockX, blockZ, Blocks.SAND);
                    position = new BlockPos(blockX, y, blockZ);
                    LargePlants.genCactus(world, position, rand, 1);
                }
            }
        }
    }

    private static void genStones(Random rand, World world, BlockPos pos) {
        modWorld.generateOre(STONES[rand.nextInt(3)], world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Blocks.STONE);
    }

    // does the tree have at least a 7 x 7 area to spawn in?
    private static boolean hasSpaceToSpawn(BlockPos pos, World world) {
        int minX = pos.getX() - 3;
        int minZ = pos.getZ() - 3;
        int maxX = pos.getX() + 3;
        int maxZ = pos.getZ() + 3;

        for (int x = minX; x <= maxX; x++) {
            for (int z = minZ; z <= maxZ; z++) {
                for (int y = pos.getY(); y <= pos.getY() + 10; y++) {
                    BlockPos testpos = new BlockPos(x,y,z);
                    if (world.getBlockState(testpos).getBlock() instanceof BlockLog)
                        return false;
                }
            }
        }
        return true;
    }

    private static void genSnow(World world, Chunk chunk) {
        for(int x = 0; x < 16; x++) {
            for(int z = 0; z < 16; z++) {
                for(int y = 31; y < 127; y++) {
                    BlockPos pos = new BlockPos(x, y, z);
                    Biome biome = chunk.getBiome(pos, world.getBiomeProvider());
                    Block blockAbove = chunk.getBlockState(x,y + 1, z).getBlock();
                    Block block = chunk.getBlockState(x, y, z).getBlock();

                    if (biome instanceof BiomeTaiga || biome == Biomes.STONE_BEACH || biome == BiomeRegistry.snowyRockyDesert || biome == BiomeRegistry.snowyRockyDesertHills) {
                        if ((block == Blocks.GRASS || block == Blocks.STONE || block == Blocks.DIRT || block == Blocks.GRAVEL || block == Blocks.LEAVES || block == Blocks.LEAVES2) && blockAbove == Blocks.AIR) {
                            chunk.setBlockState(pos.up(), Blocks.SNOW_LAYER.getDefaultState());
                        }
                    }
                }
            }
        }
    }

    private static void genGlowStone(Random rand, World world, BlockPos pos) {
        modWorld.generateOre(GLOWSTONE, world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Blocks.GRASS);
        modWorld.generateOre(GLOWSTONE, world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Blocks.MYCELIUM);
        modWorld.generateOre(GLOWSTONE, world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Blocks.DIRT);
        modWorld.generateOre(GLOWSTONE, world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Blocks.SAND);
        modWorld.generateOre(GLOWSTONE, world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Blocks.STONE);
        modWorld.generateOre(SEA_LANTERN, world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Blocks.PRISMARINE);
        modWorld.generateOre(GLOWSTONE, world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Blocks.HARDENED_CLAY);
        modWorld.generateOre(GLOWSTONE, world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Blocks.STAINED_HARDENED_CLAY);
        modWorld.generateOre(Misc.glowingIce.getDefaultState(), world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Blocks.PACKED_ICE);
        modWorld.generateOre(Misc.SaltLamp.getDefaultState(), world, rand, pos.getX(), pos.getZ(), 0, 127, 30, 10, Ores.saltBlock2);
    }

    private static void genFoliage(Random rand, Chunk chunk) {
        for(int x = 0; x < 16; x++) {
            for(int z = 0; z < 16; z++) {
                for(int y = 31; y < 127; y++) {
                    IBlockState plant = Blocks.TALLGRASS.getDefaultState().withProperty(BlockTallGrass.TYPE, BlockTallGrass.EnumType.GRASS);

                    switch (rand.nextInt(20)) {
                        case 1:
                            plant = Blocks.RED_MUSHROOM.getDefaultState();
                            break;

                        case 2:
                            plant = Blocks.BROWN_MUSHROOM.getDefaultState();
                            break;

                        case 3:
                            plant = Decoration.Rose.getDefaultState();
                            break;

                        case 4:
                            plant = Decoration.BlueRose.getDefaultState();
                            break;
                    }

                    BlockPos pos = new BlockPos(x, y, z);
                    Block blockAbove = chunk.getBlockState(pos.up()).getBlock();
                    Block block = chunk.getBlockState(pos).getBlock();

                    if (block == Blocks.GRASS && blockAbove == Blocks.AIR && rand.nextInt(5) == 0) {
                        chunk.setBlockState(pos.up(), plant);
                    }
                }
            }
        }
    }
}