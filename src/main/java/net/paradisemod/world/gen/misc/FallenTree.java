package net.paradisemod.world.gen.misc;

import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeForest;
import net.minecraft.world.biome.BiomeJungle;
import net.minecraft.world.biome.BiomeTaiga;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.paradisemod.world.biome.BiomeTemperateJungle;
import net.paradisemod.world.modWorld;

import java.util.Random;

public class FallenTree implements IWorldGenerator {
    // valid log types
    public static final IBlockState[] logs = {
            Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.BIRCH),
            Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.SPRUCE),
            Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.JUNGLE),
            Blocks.LOG.getDefaultState()
    };

    @Override
    public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        int blockX = chunkX * 16;
        int blockZ = chunkZ * 16;
        int blockY = modWorld.getGroundFromAbove(world, 31, 255, blockX, blockZ, modWorld.ground);

        // position of the stump
        BlockPos pos = new BlockPos(blockX,blockY,blockZ);

        // length of the log
        int length = 4 + rand.nextInt(3);

        // biome of the stump
        Biome biome = world.getBiomeForCoordsBody(pos);

        // default log
        IBlockState log = Blocks.LOG.getDefaultState();

        // should it generate along the x or z axis?
        boolean isZ = rand.nextBoolean();

        // should the stump be on the other side?
        boolean otherSide = rand.nextBoolean();

        // distance of the log from the stump
        int distance = 2 + rand.nextInt(2);

        // is this a valid biome?
        if (biome instanceof BiomeForest) {
            if (rand.nextBoolean() || biome == Biomes.BIRCH_FOREST || biome == Biomes.BIRCH_FOREST_HILLS || biome == Biomes.MUTATED_BIRCH_FOREST || biome == Biomes.MUTATED_BIRCH_FOREST_HILLS)
                log = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.BIRCH);
        }
        else if (biome instanceof BiomeJungle)
            log = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.JUNGLE);
        else if (biome instanceof BiomeTaiga)
            log = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.SPRUCE);
        else if (biome instanceof BiomeTemperateJungle)
            log = logs[rand.nextInt(4)];
        else
            return;

        // is the position valid?
        BlockPos newPos;
        for (int a = 0; a < length + distance; a++){
            if (isZ)
                // shorthand if statement
                newPos = (otherSide) ? pos.up().north(a) : pos.up().south(a);
            else
                newPos = (otherSide) ? pos.up().west(a) : pos.up().east(a);
            if (!modWorld.isPosEmpty(world, newPos) || modWorld.isPosEmpty(world, newPos.down()))
                return;
        }

        // generate the stump
        world.setBlockState(pos,Blocks.DIRT.getDefaultState());
        world.setBlockState(pos.up(), log);

        // generate vines around the stump
        if (modWorld.isPosEmpty(world, pos.west().up()))
            world.setBlockState(pos.west().up(),Blocks.VINE.getDefaultState().withProperty(BlockVine.EAST, true));

        if (modWorld.isPosEmpty(world, pos.east().up()))
            world.setBlockState(pos.east().up(),Blocks.VINE.getDefaultState().withProperty(BlockVine.WEST, true));

        if (modWorld.isPosEmpty(world, pos.north().up()))
            world.setBlockState(pos.north().up(),Blocks.VINE.getDefaultState().withProperty(BlockVine.SOUTH, true));

        if (modWorld.isPosEmpty(world, pos.south().up()))
            world.setBlockState(pos.south().up(),Blocks.VINE.getDefaultState().withProperty(BlockVine.NORTH, true));

        // generate the log
        BlockPos logpos;
        for (int a = distance; a < length + distance; a++){
            if (isZ) {
                logpos = (otherSide) ? pos.up().north(a) : pos.up().south(a);
                world.setBlockState(logpos, log.withProperty(BlockLog.LOG_AXIS, BlockLog.EnumAxis.Z));
            }
            else {
                logpos = (otherSide) ? pos.up().west(a) : pos.up().east(a);
                world.setBlockState(logpos, log.withProperty(BlockLog.LOG_AXIS, BlockLog.EnumAxis.X));
            }
            if (world.getBlockState(logpos.down()).getBlock() == Blocks.GRASS)
                world.setBlockState(logpos.down(), Blocks.DIRT.getDefaultState());
        }
    }
}