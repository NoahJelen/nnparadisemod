package net.paradisemod.world.gen.misc;

import net.minecraft.block.Block;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.paradisemod.base.Utils;
import net.paradisemod.world.modWorld;

import java.util.Random;

public class newIceSpike implements IWorldGenerator {
    @Override
    public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        int blockX = chunkX * 16 + rand.nextInt(8);
        int blockZ = chunkZ * 16 + rand.nextInt(8);
        int blockY = getLowestBlock(world, blockX, blockZ);
        BlockPos pos = new BlockPos(blockX, blockY, blockZ);
        if (world.getBiome(pos) != Biomes.MUTATED_ICE_FLATS || world.provider.getDimension() != 0) return;
        if (rand.nextInt(3) == 0) {
            for (int x = -4; x < 4; x++) {
                for (int z = -4; z < 4; z++) {
                    for (int y = 1; y <= genFunc(rand, x, z); y ++)
                        world.setBlockState(pos.add(x, y - 1, z), Blocks.PACKED_ICE.getDefaultState());
                }
            }
        }
    }

    private int genFunc(Random rand, int x, int y) {
        float a = (float) 100 + rand.nextInt(200);
        float fx = (float) x;
        float fy = (float) y;

        float fz = (float) (a * Math.cos((Math.pow(fx,2F) + Math.pow(fy, 2)) / 20) / (Math.pow(fx,2) + Math.pow(fy,2) + 1));
        return (int) fz;
    }

    // get the lowest block in the area
    private static int getLowestBlock(net.minecraft.world.World world, int blockX, int blockZ) {
        Block[] ground = {Blocks.DIRT, Blocks.SAND, Blocks.GRAVEL, Blocks.PACKED_ICE, Blocks.SNOW};
        int[] heights = new int[64];
        int index = 0;

        for(int x = -4; x < 4; x++) {
            for (int z = -4; z < 4; z++) {
                int y = modWorld.getGroundFromAbove(world, 61, 255, blockX + x, blockZ + z, ground);
                heights[index] = y;
                index++;
            }
        }
        return Utils.getMinValue(heights);
    }
}