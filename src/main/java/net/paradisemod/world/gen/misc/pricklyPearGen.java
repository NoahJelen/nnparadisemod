package net.paradisemod.world.gen.misc;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeDesert;
import net.minecraft.world.biome.BiomeMesa;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.paradisemod.world.biome.BiomeRockyDesert;
import net.paradisemod.world.biome.BiomeVolcanic;
import net.paradisemod.world.modWorld;

import java.util.Random;

public class pricklyPearGen implements IWorldGenerator {
    private static final Block[] genBlocks = {Blocks.GRASS, Blocks.DIRT, Blocks.SAND, Blocks.GRAVEL, Blocks.STONE, Blocks.SANDSTONE, Blocks.STAINED_HARDENED_CLAY, Blocks.HARDENED_CLAY};

    @Override
    public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (rand.nextBoolean()) return;
        int[] heights = {127, 80, 60};
        int x = (chunkX * 16) + rand.nextInt(8);
        int z = (chunkZ * 16) + rand.nextInt(8);
        int y = modWorld.getGroundFromAbove(world, 31, heights[rand.nextInt(3)], x, z, genBlocks);
        BlockPos pos = new BlockPos(x,y,z);
        Biome biome = world.getBiome(pos);

        if (!(biome instanceof BiomeDesert || biome instanceof BiomeRockyDesert || biome instanceof BiomeMesa || biome instanceof BiomeVolcanic)) return;

        if (biome instanceof BiomeMesa)
            LargePlants.genPricklyPear(world,pos,rand, 1);
        else
            LargePlants.genPricklyPear(world,pos,rand, 0);
    }
}
