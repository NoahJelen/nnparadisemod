package net.paradisemod.world.gen.misc;

import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.feature.*;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.decoration.blocks.pricklyPear;
import net.paradisemod.world.modWorld;

import java.util.Arrays;
import java.util.Random;

public class LargePlants {
	// prickly pear cactus
	public static void genPricklyPear(World world, BlockPos pos, Random rand, int sandType) {
		IBlockState[] sand = {
				Blocks.SAND.getDefaultState(),
				Blocks.SAND.getDefaultState().withProperty(BlockSand.VARIANT, BlockSand.EnumType.RED_SAND)
		};
		EnumFacing.Axis[] axes = {EnumFacing.Axis.X,EnumFacing.Axis.Z};

		if (!Arrays.asList(modWorld.ground).contains(world.getBlockState(pos).getBlock())) world.setBlockState(pos, sand[sandType]);
		world.setBlockState(pos.up(), Decoration.pricklyPear.getDefaultState().withProperty(pricklyPear.AXIS, axes[rand.nextInt(2)]).withProperty(pricklyPear.HAS_FRUIT, rand.nextBoolean()));

		BlockPos newpos = pos.up();

		for (int i = 0; i < 5; i++) {
			for (int x = -1; x <= 1; x++) {
				for (int z = -1; z <= 1; z++) {
					for (int y = 0; y <= 1; y++) {
						IBlockState state  = world.getBlockState(newpos.add(x,y,z));
						if (state.getBlock() instanceof pricklyPear)
							state.getBlock().updateTick(world,newpos.add(x,y,z),state,rand);
					}
				}
			}
		}
	}

	// cactus
	public static void genCactus(net.minecraft.world.World world, BlockPos pos, Random rand, int sandType) {
		IBlockState[] sand = {
				Blocks.SAND.getDefaultState(),
				Blocks.SAND.getDefaultState().withProperty(BlockSand.VARIANT, BlockSand.EnumType.RED_SAND)
		};
		if (modWorld.isPosEmpty(world, pos.west().up(), true) && modWorld.isPosEmpty(world, pos.east().up(), true) && modWorld.isPosEmpty(world, pos.north().up(), true) && modWorld.isPosEmpty(world, pos.south().up(), true))
			world.setBlockState(pos, sand[sandType]);
		int height = rand.nextInt(3);
		for (int y = 0; y <= height; y++)
			if (modWorld.isPosEmpty(world,pos.west().up(y+1), true)&& modWorld.isPosEmpty(world,pos.east().up(y+1), true)&& modWorld.isPosEmpty(world,pos.north().up(y+1), true)&& modWorld.isPosEmpty(world,pos.south().up(y+1), true)) {
				world.setBlockState(pos.up(y + 1), Blocks.CACTUS.getDefaultState());
			}
			else
				return;
	}

	public static void genCactus(Chunk chunk, BlockPos pos, Random rand, int sandType) {
		IBlockState[] sand = {
				Blocks.SAND.getDefaultState(),
				Blocks.SAND.getDefaultState().withProperty(BlockSand.VARIANT, BlockSand.EnumType.RED_SAND)
		};
		if (modWorld.isPosEmpty(chunk, pos.west().up(), true) && modWorld.isPosEmpty(chunk, pos.east().up(), true) && modWorld.isPosEmpty(chunk, pos.north().up(), true) && modWorld.isPosEmpty(chunk, pos.south().up(), true))
			chunk.setBlockState(pos, sand[sandType]);
		int height = rand.nextInt(3);
		for (int y=0;y<=height;y++)
			if (modWorld.isPosEmpty(chunk, pos.west().up(y + 1), true) && modWorld.isPosEmpty(chunk, pos.east().up(y + 1), true) && modWorld.isPosEmpty(chunk, pos.north().up(y + 1), true) && modWorld.isPosEmpty(chunk, pos.south().up(y + 1), true))
				chunk.setBlockState(pos.up(y + 1), Blocks.CACTUS.getDefaultState());
			else
				return;
	}

	// acacia trees
	public static void genAcaciaTree(net.minecraft.world.World world, Random rand, BlockPos pos) {
		WorldGenSavannaTree tree = new WorldGenSavannaTree(false);
		tree.generate(world,rand,pos);
	}
	
	// dark oak trees
	public static void genBigOakTree(net.minecraft.world.World world, BlockPos pos, Random rand) {
		WorldGenCanopyTree tree = new WorldGenCanopyTree(false);

		tree.generate(world,rand,pos);
	}

	// jungle bushes
	public static void genJungleBush(net.minecraft.world.World world, Random rand, BlockPos pos) {
		IBlockState log = Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.JUNGLE);
		IBlockState leaves = Blocks.LEAVES.getDefaultState()
			.withProperty(BlockLeaves.CHECK_DECAY, false)
			.withProperty(BlockLeaves.DECAYABLE, true);

		WorldGenShrub bush = new WorldGenShrub(log,leaves);

		bush.generate(world,rand,pos);
	}

	// oak, birch, and jungle trees
	public static void genClassicTree(net.minecraft.world.World world, Random rand, BlockPos pos, int type, boolean genVines) {
		int height = 5 + rand.nextInt(3);
		if (type == 2)
			height = 6 + rand.nextInt(4);
		IBlockState[] logs = {
				Blocks.LOG.getDefaultState(),
				Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.BIRCH),
				Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.JUNGLE)
		};
		IBlockState leaves[] = {
				Blocks.LEAVES.getDefaultState().withProperty(BlockLeaves.CHECK_DECAY, false).withProperty(BlockLeaves.DECAYABLE, true),
				Blocks.LEAVES.getDefaultState().withProperty(BlockOldLeaf.VARIANT, BlockPlanks.EnumType.BIRCH).withProperty(BlockLeaves.CHECK_DECAY, false).withProperty(BlockLeaves.DECAYABLE, true),
				Blocks.LEAVES.getDefaultState().withProperty(BlockOldLeaf.VARIANT, BlockPlanks.EnumType.JUNGLE).withProperty(BlockLeaves.CHECK_DECAY, false).withProperty(BlockLeaves.DECAYABLE, true)
		};

		WorldGenTrees tree = new WorldGenTrees(false,height,logs[type],leaves[type],genVines);

		tree.generate(world, rand,pos);
	}
	
	// spruce trees
	public static void genSpruceTree(net.minecraft.world.World world, Random rand, BlockPos pos) {
		WorldGenTaiga1 spruce = new WorldGenTaiga1();
		spruce.generate(world,rand,pos);
	}
}