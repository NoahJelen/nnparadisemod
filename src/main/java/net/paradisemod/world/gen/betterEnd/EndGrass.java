package net.paradisemod.world.gen.betterEnd;

import net.paradisemod.base.ModConfig;
import net.paradisemod.decoration.Decoration;
import net.paradisemod.world.Ores;
import net.paradisemod.world.modWorld;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class EndGrass implements IWorldGenerator {

	@Override
	public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
		if (world.provider.getDimension() != 1 || ModConfig.worldgen.endSurprise)
			return;
		Chunk chunk = world.getChunkFromChunkCoords(chunkX, chunkZ);
		Block[] plants= {Decoration.EnderRose, modWorld.EndGrass, modWorld.TallEndGrass};

		for (int x = 0; x < 16; x++) {
			for (int z = 0; z < 16; z++) {
				for (int y = 0; y < 80; y++) {
					BlockPos pos = new BlockPos(x,y,z);

					// the block to be replaced
					Block blockToReplace = chunk.getBlockState(pos).getBlock();

					// the block above it
					Block blockAbove = chunk.getBlockState(pos.up()).getBlock();

					// replace exposed end stone
					if ((blockToReplace == Blocks.END_STONE||blockToReplace == Ores.EnderPearlOre||blockToReplace == Ores.EndRubyOre||blockToReplace == Ores.VoidPearlOre) && blockAbove==Blocks.AIR)
						chunk.setBlockState(pos, modWorld.OvergrownEndStone.getDefaultState());
					
					// ender roses and grass
					if (rand.nextInt(15)==0 && (blockToReplace == Blocks.END_STONE||blockToReplace==modWorld.OvergrownEndStone) && blockAbove==Blocks.AIR)
						chunk.setBlockState(pos.up(), plants[rand.nextInt(3)].getDefaultState());
				}
			}
		}
	}
}