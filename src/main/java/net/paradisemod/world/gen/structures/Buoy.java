package net.paradisemod.world.gen.structures;

import net.paradisemod.building.Fences;
import net.paradisemod.building.Gates;
import net.paradisemod.building.Trapdoors;
import net.paradisemod.base.ModConfig;
import net.paradisemod.world.Ores;
import net.paradisemod.world.modWorld;
import net.paradisemod.redstone.Lamps;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFenceGate;
import net.minecraft.block.BlockLever;
import net.minecraft.block.BlockTrapDoor;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class Buoy implements IWorldGenerator{

	@Override
	public void generate(Random rand, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator,
						 IChunkProvider chunkProvider) {
		if(ModConfig.worldgen.structures.Buoys==false)
			return;
		int blockX = chunkX * 16;
		int blockZ = chunkZ * 16;
		int y = modWorld.getGroundFromAbove(world, 60, 255, blockX, blockZ, new Block[] {Blocks.WATER, Blocks.ICE});

		if(rand.nextInt(ModConfig.worldgen.structures.BuoysChance) == 0){
			if (y<=60)
				return;
			BlockPos pos = new BlockPos(blockX, y, blockZ);
			IBlockState iblockstate = world.getBlockState(pos);
			world.notifyBlockUpdate(pos, iblockstate, iblockstate, 3);
			for (int x=0;x<=5;x++) {
				for (int z = 0; z <= 5; z++)
					world.setBlockState(pos.add(x, 1, z), Fences.RustyFence.getDefaultState());
			}
			for (int x=1;x<=4;x++) {
				for (int z = 1; z <= 4; z++)
					world.setBlockState(pos.add(x, 1, z), Blocks.AIR.getDefaultState());
			}

			for (int x=0;x<=5;x++) {
				for (int z = 0; z <= 5; z++)
					world.setBlockState(pos.add(x, 0, z), Trapdoors.RustyTrapdoor.getDefaultState().withProperty(BlockTrapDoor.HALF, BlockTrapDoor.DoorHalf.TOP));
			}

			world.setBlockState(pos.add(2,1,5), Gates.RustyFenceGate.getDefaultState().withProperty(BlockFenceGate.FACING, EnumFacing.NORTH));

			Block lamp = getLamp(rand);
			for(int x=2;x<=3;x++) {
				for (int z = 2; z <= 3; z++) {
					for (int by = 1; by <= 2; by++)
						world.setBlockState(pos.add(x,by,z), lamp.getDefaultState());
				}
			}

			for(int x=2;x<=3;x++) {
				for (int z = 2; z <= 3; z++)
					world.setBlockState(pos.add(x, 3, z), lever(world));
			}

			for(int x=2;x<=3;x++) {
				for (int z = 2; z <= 3; z++) {
					for (int by = -1; by <= 0; by++)
						world.setBlockState(pos.add(x,by,z), Ores.RustyIronBlock.getDefaultState());
				}
			}
		}
	}

	private static Block getLamp(Random rand) {
		int lampNum = rand.nextInt(18);
		if (lampNum < 17) {
			return Lamps.lit_lamps[lampNum];
		}
		return Blocks.LIT_REDSTONE_LAMP;
	}
	private static IBlockState lever(net.minecraft.world.World world){
		Random rand = world.rand;
		if (rand.nextBoolean())
			return Blocks.LEVER.getDefaultState().withProperty(BlockLever.POWERED, Boolean.TRUE).withProperty(BlockLever.FACING,BlockLever.EnumOrientation.UP_Z);
		return Blocks.LEVER.getDefaultState().withProperty(BlockLever.POWERED, Boolean.TRUE).withProperty(BlockLever.FACING,BlockLever.EnumOrientation.UP_X);
	}
}