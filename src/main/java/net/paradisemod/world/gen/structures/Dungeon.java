package net.paradisemod.world.gen.structures;

import net.paradisemod.base.ModConfig;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeMesa;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.paradisemod.world.gen.structures.Dungeons.BrickPyramid;
import net.paradisemod.world.gen.structures.Dungeons.Minerbase;
import net.paradisemod.world.gen.structures.Dungeons.MiniStronghold;
import net.paradisemod.world.gen.structures.Dungeons.VoidDungeonSmall;
import net.paradisemod.world.gen.structures.Dungeons.VoidDungeonMedium;
import net.paradisemod.world.gen.structures.Dungeons.VoidDungeonLarge;
import net.paradisemod.world.gen.structures.Dungeons.VoidTower;
import net.paradisemod.world.gen.structures.Dungeons.MesaTemple;

import java.util.Random;

public class Dungeon implements IWorldGenerator {
    //dungeons found in the overworld
    public static IWorldGenerator[] dungeons = {new BrickPyramid(),new Minerbase(),new MiniStronghold()};

    //dungeons found in the deep void
    public static IWorldGenerator[] dungeons_deep_void = {new VoidDungeonSmall(),new VoidDungeonMedium(),new VoidDungeonLarge(),new VoidTower()};

    //mesa temple
    //only found in mesa biomes
    public static IWorldGenerator mesa_temple = new MesaTemple();

    @Override
    public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {

        //current dimension
        int dim = world.provider.getDimension();

        //the chunk's coordinates as a block position
        BlockPos pos = new BlockPos(chunkX*16,0,chunkZ*16);

        //get the biome of the chunk
        Biome biome = world.getBiome(pos);

        //generate the mesa temple
        if (biome instanceof BiomeMesa){
            mesa_temple.generate(rand, chunkX,chunkZ,world,chunkGenerator,chunkProvider);
        }

        //generate the deep void dungeons
        if (dim == ModConfig.dimensions.DeepVoidDim)
            dungeons_deep_void[rand.nextInt(4)].generate(rand, chunkX,chunkZ,world,chunkGenerator,chunkProvider);

        //generate the other dungeons
        else
            dungeons[rand.nextInt(3)].generate(rand, chunkX,chunkZ,world,chunkGenerator,chunkProvider);
    }
}