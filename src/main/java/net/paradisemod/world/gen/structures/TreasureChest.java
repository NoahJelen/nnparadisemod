package net.paradisemod.world.gen.structures;

import java.util.Random;

import net.paradisemod.base.ModConfig;

import net.paradisemod.world.modWorld;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class TreasureChest implements IWorldGenerator{
	@Override
	public void generate(Random rand, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator,
						 IChunkProvider chunkProvider) {
		if(ModConfig.worldgen.structures.TreasureChest==false)
			return;
		int blockX = chunkX * 16;
		int blockZ = chunkZ * 16;	
		int y = modWorld.getGroundFromAbove(world, 31, 255, blockX, blockZ, modWorld.ground)-2;
		BlockPos pos = new BlockPos(blockX, y, blockZ);

		if (y>31) {
			if(rand.nextInt(ModConfig.worldgen.structures.TreasureChestChance) == 0){
				world.setBlockState(pos, Blocks.CHEST.getDefaultState());
				world.setBlockState(pos.west(), Blocks.CHEST.getDefaultState());

				TileEntityChest chest1 = (TileEntityChest) world.getTileEntity(pos);
				chest1.setLootTable(new ResourceLocation("nnparadisemod:treasure_chest"),rand.nextLong());

				TileEntityChest chest2 = (TileEntityChest) world.getTileEntity(pos.west());
				chest2.setLootTable(new ResourceLocation("nnparadisemod:treasure_chest"),rand.nextLong());
			}
		}
	}
}