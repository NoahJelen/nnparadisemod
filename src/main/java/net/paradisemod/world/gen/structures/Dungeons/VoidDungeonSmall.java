package net.paradisemod.world.gen.structures.Dungeons;

import net.paradisemod.building.Building;
import net.paradisemod.base.ModConfig;
import net.paradisemod.world.modWorld;
import net.minecraft.block.BlockChest;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityMobSpawner;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class VoidDungeonSmall implements IWorldGenerator {
    //possible mobs for mob spawner
    public String[] mobs = {"zombie","creeper","spider","cave_spider","skeleton"};

    @Override
    public void generate(Random random, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (ModConfig.worldgen.structures.VoidDungeon==false)
            return;
        int blockX = chunkX * 16;
        int blockZ = chunkZ * 16;
        int blockY = modWorld.getGroundFromAbove(world, 31, 120, blockX, blockZ, Building.VoidStone);
        if (blockY<=31)
            return;
        BlockPos pos = new BlockPos(blockX, blockY-17, blockZ);

        //generate the walls of the dungeon
        for (int x=0;x<7;x++){
            for (int y=0;y<7;y++){
                for (int z=0;z<7;z++){
                    if (world.getBlockState(pos.add(x,y,z)).getBlock()!= Blocks.AIR)
                        world.setBlockState(pos.add(x,y,z),Building.VoidBricks.getDefaultState());
                }
            }
        }
        for (int x=1;x<6;x++){
            for (int y=1;y<6;y++){
                for (int z=1;z<6;z++)
                    world.setBlockState(pos.add(x,y,z),Blocks.AIR.getDefaultState());
            }
        }

        //bottom platform
        for (int x=0;x<7;x++){
            for (int z=0;z<7;z++)
                world.setBlockState(pos.add(x,0,z),Building.VoidBricks.getDefaultState());
        }

        //chest 1
        world.setBlockState(pos.add(1,1,1),Blocks.CHEST.getDefaultState().withProperty(BlockChest.FACING, EnumFacing.SOUTH));
        TileEntityChest chest1 = (TileEntityChest) world.getTileEntity(pos.add(1,1,1));
        chest1.setLootTable(LootTableList.CHESTS_SIMPLE_DUNGEON, random.nextLong());

        //chest 2
        world.setBlockState(pos.add(2,1,1),Blocks.CHEST.getDefaultState().withProperty(BlockChest.FACING, EnumFacing.SOUTH));
        TileEntityChest chest2 = (TileEntityChest) world.getTileEntity(pos.add(2,1,1));
        chest2.setLootTable(LootTableList.CHESTS_SIMPLE_DUNGEON, random.nextLong());

        //chest 3
        world.setBlockState(pos.add(5,1,5),Blocks.CHEST.getDefaultState().withProperty(BlockChest.FACING, EnumFacing.WEST));
        TileEntityChest chest3 = (TileEntityChest) world.getTileEntity(pos.add(5,1,5));
        chest3.setLootTable(LootTableList.CHESTS_SIMPLE_DUNGEON, random.nextLong());

        //chest 4
        world.setBlockState(pos.add(5,1,4),Blocks.CHEST.getDefaultState().withProperty(BlockChest.FACING, EnumFacing.WEST));
        TileEntityChest chest4 = (TileEntityChest) world.getTileEntity(pos.add(5,1,4));
        chest4.setLootTable(LootTableList.CHESTS_SIMPLE_DUNGEON, random.nextLong());

        //mob spawner
        world.setBlockState(pos.add(3,1,3),Blocks.MOB_SPAWNER.getDefaultState());
        TileEntityMobSpawner spawner = (TileEntityMobSpawner) world.getTileEntity(pos.add(3,1,3));
        spawner.getSpawnerBaseLogic().setEntityId(new ResourceLocation("minecraft:"+mobs[random.nextInt(5)]));
    }
}