package net.paradisemod.world.gen.caveGen;

import net.paradisemod.world.blocks.Crystal;
import net.paradisemod.base.ModConfig;
import net.paradisemod.world.Crystals;
import net.paradisemod.world.biome.BiomeSaltFlat;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockDirectional;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class CaveGenCrystal implements IWorldGenerator {
	public static final Block[] crystals = { Crystals.diamondCrystal, Crystals.emeraldCrystal, Crystals.quartzCrystal,
			Crystals.redstoneCrystal, Crystals.rubyCrystal, Crystals.saltCrystal};
	public static final EnumFacing[] directions = {EnumFacing.UP, EnumFacing.DOWN, EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.EAST, EnumFacing.WEST};

	@Override
	public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
		if (rand.nextInt(ModConfig.worldgen.caves.CrystalChance) == 0) {
			int dim = world.provider.getDimension();
			int rolls = 1024;
			if (dim == -2 || dim == -3 || !world.provider.isSurfaceWorld())
				rolls = 8192;

			Block crystal = crystals[rand.nextInt(6)];
			for(EnumFacing direction : directions) {
				for (int i = 0; i < rolls; i++) {
					int x = (chunkX * 16) + rand.nextInt(32);
					int y = 9 + rand.nextInt(31);
					switch (dim) {
						case -1:
							crystal = Crystals.quartzCrystal;
							y = rand.nextInt(128);
							break;
						case -2:
							y = rand.nextInt(128);
							break;
						case -3:
							y = rand.nextInt(90);
							break;
						default:
							crystal = crystals[rand.nextInt(6)];
					}
					int z = (chunkZ * 16) + rand.nextInt(32);
					BlockPos pos = new BlockPos(x,y,z);
					BlockPos next_block = getBasePos(direction, pos);

					if (world.getBiome(pos) instanceof BiomeSaltFlat)
						crystal = Crystals.saltCrystal;

					if (world.getBlockState(next_block).isTopSolid() && world.getBlockState(pos).getBlock() instanceof BlockAir)
						world.setBlockState(pos,crystal.getDefaultState().withProperty(BlockDirectional.FACING,direction.getOpposite()).withProperty(Crystal.TYPE,rand.nextInt(4)));
				}
			}
		}
	}

	private BlockPos getBasePos(EnumFacing direction, BlockPos pos) {
		switch (direction) {
			case UP:
				return pos.up();
			case DOWN:
				return pos.down();
			case NORTH:
				return pos.north();
			case SOUTH:
				return pos.south();
			case EAST:
				return pos.east();
			case WEST:
				return pos.west();
		}
		return pos.up();
	}
}