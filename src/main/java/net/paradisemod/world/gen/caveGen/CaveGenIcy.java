package net.paradisemod.world.gen.caveGen;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDirectional;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Biomes;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeSnow;
import net.minecraft.world.chunk.Chunk;
import net.paradisemod.base.ModConfig;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.biome.BiomeGlacier;
import net.paradisemod.world.biome.BiomeRegistry;
import net.paradisemod.world.modWorld;

import java.util.Random;

public class CaveGenIcy {
	protected static final IBlockState PACKED_ICE = Blocks.PACKED_ICE.getDefaultState();
	//public static int height;

	public static void generate(Random rand, BlockPos pos, Chunk chunk, World world) {
		if (!ModConfig.worldgen.caves.types.Icy) return;
		// biome of current block
		Biome biome = chunk.getBiome(pos, world.getBiomeProvider());

		// the block to be replaced
		Block blockToReplace = chunk.getBlockState(pos).getBlock();

		// the block above it
		Block blockAbove = chunk.getBlockState(pos.up()).getBlock();

		// the block below it
		Block blockBelow = chunk.getBlockState(pos.down()).getBlock();
		if (
				(
						blockToReplace == Blocks.STONE
								|| blockToReplace == Blocks.DIRT
								|| blockToReplace == Blocks.PACKED_ICE
				)
						&&(
						biome instanceof BiomeSnow
								|| biome == Biomes.FROZEN_RIVER
								|| biome == Biomes.COLD_TAIGA
								|| biome == Biomes.COLD_TAIGA_HILLS
								|| biome == Biomes.COLD_BEACH
								|| biome instanceof BiomeGlacier
								|| biome == BiomeRegistry.snowyRockyDesert
								|| biome == BiomeRegistry.snowyRockyDesertHills
				)
		) {
			// replace exposed stone
			if (chunk.getBlockState(pos.up()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.down()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.south()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.west()).getBlock() == Blocks.AIR
					||chunk.getBlockState(pos.east()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.north()).getBlock() == Blocks.AIR

					|| chunk.getBlockState(pos.east()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.up()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.south()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.west()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.down()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.north()).getBlock() == Blocks.WATER

					|| chunk.getBlockState(pos.east()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.up()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.south()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.west()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.down()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.north()).getBlock() == Blocks.LAVA) {

				//icicles
				if (blockAbove==Blocks.AIR&&rand.nextInt(10)==0)
					chunk.setBlockState(pos.up(), modWorld.icicle.getDefaultState());
				if (blockBelow==Blocks.AIR&&rand.nextInt(10)==0&&pos.getY()>0)
					chunk.setBlockState(pos.down(), modWorld.icicle.getDefaultState().withProperty(BlockDirectional.FACING, EnumFacing.DOWN));

				// ice cave generation
				switch (rand.nextInt(5)) {
					//packed ice
					case 1:
						chunk.setBlockState(pos, PACKED_ICE);
						break;
					//snow
					case 4:
						if(blockAbove==Blocks.AIR)
							chunk.setBlockState(pos.up(),Blocks.SNOW_LAYER.getDefaultState());
						break;
				}
			}

			//icicles
			if (blockToReplace == Misc.blueIce&&biome instanceof BiomeGlacier) {
				if (blockAbove==Blocks.AIR&&rand.nextInt(10)==0)
					chunk.setBlockState(pos.up(), modWorld.icicle.getDefaultState());
				if (blockBelow==Blocks.AIR&&rand.nextInt(10)==0&&pos.getY()>0)
					chunk.setBlockState(pos.down(), modWorld.icicle.getDefaultState().withProperty(BlockDirectional.FACING, EnumFacing.DOWN));
			}
		}
	}
}