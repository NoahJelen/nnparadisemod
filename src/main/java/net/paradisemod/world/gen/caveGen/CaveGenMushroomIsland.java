package net.paradisemod.world.gen.caveGen;

import net.minecraft.block.Block;
import net.minecraft.block.BlockHugeMushroom;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeMushroomIsland;
import net.minecraft.world.chunk.Chunk;
import net.paradisemod.base.ModConfig;

import java.util.Random;

public class CaveGenMushroomIsland {

	public static void generate(Random rand, BlockPos pos, Chunk chunk, World world) {
		if (!ModConfig.worldgen.caves.types.Mushroom)
			return;

		// biome of current block
		Biome biome = chunk.getBiome(pos, world.getBiomeProvider());

		// the block to be replaced
		Block blockToReplace = chunk.getBlockState(pos).getBlock();

		// the block above it
		Block blockAbove = chunk.getBlockState(pos.up()).getBlock();

		// the block below it
		Block blockBelow = chunk.getBlockState(pos.down()).getBlock();

		if (biome instanceof BiomeMushroomIsland) {
			// replace ground stone with mycelium
			if ((blockToReplace==Blocks.STONE||blockToReplace==Blocks.COBBLESTONE)&& blockAbove == Blocks.AIR)
				chunk.setBlockState(pos, Blocks.MYCELIUM.getDefaultState());

			// little mushrooms
			if(chunk.getBlockState(pos).getBlock() == Blocks.MYCELIUM){
				switch (rand.nextInt(10)) {
					// red
					case 1:
						chunk.setBlockState(pos.up(), Blocks.RED_MUSHROOM.getDefaultState());
						break;
					// brown
					case 9:
						chunk.setBlockState(pos.up(), Blocks.BROWN_MUSHROOM.getDefaultState());
						break;
				}
			}

			// the cave is a giant mushroom!
			if ((blockToReplace==Blocks.STONE||blockToReplace==Blocks.COBBLESTONE) && blockBelow == Blocks.AIR) {
				int a = 1;
				chunk.setBlockState(pos, Blocks.BROWN_MUSHROOM_BLOCK.getDefaultState().withProperty(BlockHugeMushroom.VARIANT, BlockHugeMushroom.EnumType.CENTER));
				if (rand.nextInt(30) == 0) {
					Block blockToReplace2 = chunk.getBlockState(pos.down()).getBlock();
					while (blockToReplace2 == Blocks.AIR
							|| blockToReplace2 == Blocks.WATER
							|| blockToReplace2 == Blocks.LAVA
							|| blockToReplace2 == Blocks.FLOWING_LAVA
							|| blockToReplace2 == Blocks.FLOWING_WATER) {
						chunk.setBlockState(pos.down(a),Blocks.BROWN_MUSHROOM_BLOCK.getDefaultState().withProperty(BlockHugeMushroom.VARIANT, BlockHugeMushroom.EnumType.STEM));
						a++;
						blockToReplace2 = chunk.getBlockState(pos.down(a)).getBlock();
					}
				}
			}
		}
	}
}