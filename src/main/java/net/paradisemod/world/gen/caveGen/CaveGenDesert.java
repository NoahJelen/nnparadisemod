package net.paradisemod.world.gen.caveGen;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDirectional;
import net.minecraft.block.BlockTallGrass;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeDesert;
import net.minecraft.world.chunk.Chunk;
import net.paradisemod.base.ModConfig;
import net.paradisemod.misc.Misc;
import net.paradisemod.world.Ores;
import net.paradisemod.world.biome.BiomeRockyDesert;
import net.paradisemod.world.biome.BiomeSaltFlat;
import net.paradisemod.world.blocks.fluids.MoltenSalt.BlockMoltenSalt;
import net.paradisemod.world.gen.misc.LargePlants;
import net.paradisemod.world.modWorld;

import java.util.Random;

public class CaveGenDesert {
	protected static final IBlockState SANDSTONE = Blocks.SANDSTONE.getDefaultState();
	protected static final IBlockState SAND = Blocks.SAND.getDefaultState();

	public static void generate(Random rand, BlockPos pos, Chunk chunk, World world) {
		// don't generate if the config says not to
		// generate cave features
		if (!ModConfig.worldgen.caves.types.Dry) return;

		// biome of current block
		Biome biome = chunk.getBiome(pos, world.getBiomeProvider());

		// the block to be replaced
		Block blockToReplace = chunk.getBlockState(pos).getBlock();

		// the block above it
		Block blockAbove = chunk.getBlockState(pos.up()).getBlock();

		// the block below it
		Block blockBelow = chunk.getBlockState(pos.down()).getBlock();

		// vanilla desert
		if (biome instanceof BiomeDesert) {
			if ((blockToReplace == Blocks.STONE || blockToReplace == Blocks.SANDSTONE) && (chunk.getBlockState(pos.east()).getBlock() == Blocks.AIR
					||chunk.getBlockState(pos.up()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.down()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.south()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.west()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.east()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.north()).getBlock() == Blocks.AIR

					|| chunk.getBlockState(pos.east()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.up()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.south()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.west()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.down()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.north()).getBlock() == Blocks.WATER

					|| chunk.getBlockState(pos.east()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.up()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.south()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.west()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.down()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.north()).getBlock() == Blocks.LAVA)) {
				// lava
				if (rand.nextInt(20) == 0 && blockAbove == Blocks.AIR)
					if (!modWorld.isPosEmpty(chunk,pos.west())&&!modWorld.isPosEmpty(chunk,pos.east())&&!modWorld.isPosEmpty(chunk,pos.north())&&!modWorld.isPosEmpty(chunk,pos.south()))
						chunk.setBlockState(pos, Blocks.LAVA.getDefaultState());
				// sandstone
				if (rand.nextBoolean())
					chunk.setBlockState(pos, SANDSTONE);

				// sandstone formations
				if (blockAbove == Blocks.AIR && rand.nextInt(10) == 0)
					chunk.setBlockState(pos.up(), modWorld.sandstoneFormation.getDefaultState());
				if (blockBelow == Blocks.AIR&&rand.nextInt(10) == 0 && pos.getY() > 0)
					chunk.setBlockState(pos.down(), modWorld.sandstoneFormation.getDefaultState().withProperty(BlockDirectional.FACING, EnumFacing.DOWN));
			}

			// this needs to be reset
			blockToReplace = chunk.getBlockState(pos).getBlock();

			// desert foliage
			if (blockToReplace == Blocks.SANDSTONE || blockToReplace == Blocks.SAND || blockToReplace == Blocks.STONE && blockAbove==Blocks.AIR) {
				if (rand.nextInt(10) == 0) {
					if (rand.nextBoolean()) {
						if (modWorld.isPosEmpty(chunk, pos.up())) {
							chunk.setBlockState(pos, Blocks.SAND.getDefaultState());
							chunk.setBlockState(pos.up(), Blocks.DEADBUSH.getDefaultState());
						}
					}
					else
						LargePlants.genCactus(chunk, pos, rand, 0);
				}
			}
		}

		// salt flat
		if (biome instanceof BiomeSaltFlat){
			if ((blockToReplace == Blocks.STONE)&&(chunk.getBlockState(pos.east()).getBlock() == Blocks.AIR
					||chunk.getBlockState(pos.up()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.down()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.south()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.west()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.east()).getBlock() == Blocks.AIR
					|| chunk.getBlockState(pos.north()).getBlock() == Blocks.AIR

					|| chunk.getBlockState(pos.east()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.up()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.south()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.west()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.down()).getBlock() == Blocks.WATER
					|| chunk.getBlockState(pos.north()).getBlock() == Blocks.WATER

					|| chunk.getBlockState(pos.east()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.up()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.south()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.west()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.down()).getBlock() == Blocks.LAVA
					|| chunk.getBlockState(pos.north()).getBlock() == Blocks.LAVA))
			{
				// salt lamp
				if (rand.nextInt(20) == 0 && blockAbove == Blocks.AIR||blockToReplace == Blocks.GLOWSTONE)
					chunk.setBlockState(pos, Misc.SaltLamp.getDefaultState());

				//salt block
				if (rand.nextBoolean())
					chunk.setBlockState(pos, Ores.SaltBlock.getDefaultState());

				//molten salt
				if (rand.nextInt(20) == 0 && blockAbove == Blocks.AIR)
					if (!modWorld.isPosEmpty(chunk,pos.west())&&!modWorld.isPosEmpty(chunk,pos.east())&&!modWorld.isPosEmpty(chunk,pos.north())&&!modWorld.isPosEmpty(chunk,pos.south()))
						chunk.setBlockState(pos, BlockMoltenSalt.instance.getDefaultState());
			}

			// compact salt block
			if (blockToReplace == Blocks.STONE && blockAbove==Blocks.AIR && rand.nextBoolean())
				chunk.setBlockState(pos, Ores.saltBlock2.getDefaultState());
		}

		// rocky desert foliage
		if (biome instanceof BiomeRockyDesert) {
			if (blockToReplace == Blocks.GRASS || blockToReplace == Blocks.STONE && blockAbove == Blocks.AIR) {
				chunk.setBlockState(pos , Blocks.DIRT.getDefaultState());
				if (modWorld.isPosEmpty(chunk,pos.up()) && rand.nextInt(3) == 0) {
					switch (rand.nextInt(3)) {
						case 0:
							chunk.setBlockState(pos, Blocks.DIRT.getDefaultState());
							chunk.setBlockState(pos.up(), Blocks.DEADBUSH.getDefaultState());
							break;

						case 1:
							LargePlants.genCactus(chunk, pos, rand, 0);
							break;

						case 2:
							chunk.setBlockState(pos.up(), Blocks.TALLGRASS.getDefaultState().withProperty(BlockTallGrass.TYPE, BlockTallGrass.EnumType.GRASS));
					}
				}
			}
		}
	}
}