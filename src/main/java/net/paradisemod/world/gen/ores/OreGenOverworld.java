package net.paradisemod.world.gen.ores;

import java.util.Random;

import net.paradisemod.base.ModConfig;
import net.paradisemod.world.Ores;

import net.paradisemod.world.modWorld;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class OreGenOverworld implements IWorldGenerator {
	@Override
	public void generate(Random random, int chunkX, int chunkZ, net.minecraft.world.World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
		if ((world.provider.getDimension()==0)&& ModConfig.worldgen.GenerateOverworldOres==true){
			IBlockState[] ores= {Ores.SaltOre.getDefaultState(),Ores.RubyOre.getDefaultState(),Ores.SilverOre.getDefaultState()};
			modWorld.generateOre(ores[random.nextInt(3)],world,random,chunkX*16, chunkZ * 16, 16,64, 4 +random.nextInt(4),6,Blocks.STONE);
		}
	}
}