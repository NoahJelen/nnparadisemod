package net.paradisemod.world.blocks.fluids;

import net.paradisemod.world.blocks.fluids.EnderAcid.BlockEnderAcid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraftforge.client.model.ModelLoader;

public class fluidRenders {
	public static void register() {
		//this registers the fluids' models

		//liquid redstone
		ModelLoader.setCustomStateMapper(LiquidRedstone.BlockLiquidRedstone.instance, new net.minecraft.client.renderer.block.statemap.StateMapperBase() {
			@Override
			protected ModelResourceLocation getModelResourceLocation(IBlockState state) {
				return new ModelResourceLocation("nnparadisemod:liquid_redstone", "fluid");
			}
		});

		//ender acid
		ModelLoader.setCustomStateMapper(BlockEnderAcid.instance, new StateMapperBase() {
			@Override
			protected ModelResourceLocation getModelResourceLocation(IBlockState state) {
				return new ModelResourceLocation("nnparadisemod:ender_acid","fluid");
			}
		});

		//glowing water
		ModelLoader.setCustomStateMapper(GlowingWater.BlockGlowingWater.instance, new StateMapperBase() {
			@Override
			protected ModelResourceLocation getModelResourceLocation(IBlockState state) {
				return new ModelResourceLocation("nnparadisemod:glowing_water", "fluid");
			}
		});

		//molten salt
		ModelLoader.setCustomStateMapper(MoltenSalt.BlockMoltenSalt.instance, new StateMapperBase() {
			@Override
			protected ModelResourceLocation getModelResourceLocation(IBlockState state) {
				return new ModelResourceLocation("nnparadisemod:molten_salt", "fluid");
			}
		});
		
	}
}
