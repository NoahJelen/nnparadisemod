package net.paradisemod.world.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDirectional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.Random;

import static net.minecraft.util.EnumFacing.UP;

public class Crystal extends BlockDirectional {
    public static final PropertyInteger TYPE = PropertyInteger.create("type", 0, 3);
    public final Item dropItem;
    protected static final AxisAlignedBB[] CRYSTAL_UP = {
            new AxisAlignedBB(5D / 16D, 0D, 5D / 16D, 11D / 16D, 17.5D / 16D, 11D / 16D),
            FULL_BLOCK_AABB,
            new AxisAlignedBB(5D / 16D, 0D, 5D / 16D, 11D / 16D, 14.5D / 16D, 11D / 16D),
            new AxisAlignedBB(2D / 16D,0D,2D / 16D,14D / 16D,18.5D / 16D,14D / 16D)
    };
    protected static final AxisAlignedBB[] CRYSTAL_DOWN = {
            new AxisAlignedBB(5D / 16D, -1.5D / 16D, 5D / 16D, 11D / 16D, 1D, 11D / 16D),
            FULL_BLOCK_AABB,
            new AxisAlignedBB(5D / 16D, 1.5D / 16D, 5D / 16D, 11D / 16D, 1D, 11D / 16D),
            new AxisAlignedBB(2D / 16D,-2.5D / 16D,2D / 16D,14D / 16D,1D,14D / 16D)
    };
    protected static final AxisAlignedBB[] CRYSTAL_NORTH = {
            new AxisAlignedBB(5D / 16D, 5D / 16D, -1.5D / 16D, 11D / 16D, 11D / 16D, 1D),
            FULL_BLOCK_AABB,
            new AxisAlignedBB(5D / 16D, 5D / 16D, 1.5D / 16D, 11D / 16D, 11D / 16D, 1D),
            new AxisAlignedBB(2D/16D,2D/16D,-2.5D/16D,14D/16D,14D/16D,1D)
    };
    protected static final AxisAlignedBB[] CRYSTAL_SOUTH = {
            new AxisAlignedBB(5D / 16D, 5D / 16D, 00D, 11D / 16D, 11D / 16D, 17.5D / 16D),
            FULL_BLOCK_AABB,
            new AxisAlignedBB(5D / 16D, 5D / 16D, 00D, 11D / 16D, 11D / 16D, 14D / 16D),
            new AxisAlignedBB(2D/16D,2D/16D,0D,14D/16D,14D/16D,18.5D/16D)
    };
    protected static final AxisAlignedBB[] CRYSTAL_EAST = {
            new AxisAlignedBB(0D, 5D / 16D, 5D / 16D, 17.5D / 16D, 11D / 16D, 11D / 16D),
            FULL_BLOCK_AABB,
            new AxisAlignedBB(0D, 5D / 16D, 5D / 16D, 14D / 16D, 11D / 16D, 11D / 16D),
            new AxisAlignedBB(0D,2D / 16D,2D / 16D,18.5D / 16D,14D / 16D,14D / 16D)
    };
    protected static final AxisAlignedBB[] CRYSTAL_WEST = {
            new AxisAlignedBB(-1.5D / 16D, 5D / 16D, 5D / 16D, 1D, 11D / 16D, 11D / 16D),
            FULL_BLOCK_AABB,
            new AxisAlignedBB(1.5D / 16D, 5D / 16D, 5D / 16D, 1D, 11D / 16D, 11D / 16D),
            new AxisAlignedBB(-2.5D / 16D,2D / 16D,2D / 16D,1D,14D / 16D,14D / 16D)
    };
    private static final EnumFacing[] directions = {EnumFacing.UP,EnumFacing.DOWN,EnumFacing.NORTH,EnumFacing.SOUTH,EnumFacing.WEST,EnumFacing.EAST};

    public Crystal(String regName, String ULName, Item drop) {
        super(Material.PLANTS);
        dropItem = drop;
        setUnlocalizedName(ULName);
        setRegistryName(regName);
        setHarvestLevel("pickaxe", 0);
        setHardness(.5F);
        setResistance(1F);
        setLightLevel(1F);
        setSoundType(SoundType.GLASS);
        setTickRandomly(false);
        setCreativeTab(CreativeTabs.DECORATIONS);
        setDefaultState(this.blockState.getBaseState().withProperty(FACING, UP).withProperty(TYPE, 0));
    }

    @Override
    public boolean canPlaceBlockOnSide(World world, BlockPos pos, EnumFacing side) {
        return isSupport(world, side, pos);
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block block, BlockPos fromPos)
    {
        if (!isSupport(world, state.getValue(FACING), pos)) {
            dropBlockAsItemWithChance(world, pos, state, 1.0f, 0);
            world.setBlockState(pos, Blocks.AIR.getDefaultState());
        }
    }

    private boolean isSupport(World world, EnumFacing direction, BlockPos pos) {
        BlockPos support_pos = pos.down();
        switch (direction) {
            case UP:
                support_pos = pos.down();
                break;

            case DOWN:
                support_pos = pos.up();
                break;

            case NORTH:
                support_pos = pos.south();
                break;

            case SOUTH:
                support_pos = pos.north();
                break;

            case EAST:
                support_pos = pos.west();
                break;

            case WEST:
                support_pos = pos.east();
                break;
        }

        return world.getBlockState(support_pos).isTopSolid();
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
    {
        return new ItemStack(this);
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        int type = state.getValue(TYPE);
        EnumFacing direction = state.getValue(FACING);

        switch (direction) {
            case WEST:
                return CRYSTAL_WEST[type];
            case EAST:
                return CRYSTAL_EAST[type];
            case UP:
                return CRYSTAL_UP[type];
            case DOWN:
                return CRYSTAL_DOWN[type];
            case NORTH:
                return CRYSTAL_NORTH[type];
            case SOUTH:
                return CRYSTAL_SOUTH[type];
        }
        return FULL_BLOCK_AABB;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.TRANSLUCENT;
    }

    // get item to drop
    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return dropItem;
    }

    // get amount of items to drop
    @Override
    public int quantityDropped(Random random)
    {
        return random.nextInt(4) + 1;
    }

    // is the pick enchanted with fortune?
    @Override
    public int quantityDroppedWithBonus(int fortune, Random random)
    {
        if (fortune > 0 && Item.getItemFromBlock(this) != this.getItemDropped(this.getBlockState().getValidStates().iterator().next(), random, fortune))
        {
            int i = random.nextInt(fortune + 2) - 1;

            if (i < 0)
                i = 0;

            return this.quantityDropped(random) * (i + 1);
        }
        else
            return this.quantityDropped(random);
    }

    // spawn the item in the world
    @Override
    public void dropBlockAsItemWithChance(World worldIn, BlockPos pos, IBlockState state, float chance, int fortune)
    {
        super.dropBlockAsItemWithChance(worldIn, pos, state, chance, fortune);
    }

    // spawn xp orbs
    @Override
    public int getExpDrop(IBlockState state, net.minecraft.world.IBlockAccess world, BlockPos pos, int fortune)
    {
        Random rand = world instanceof World ? ((World)world).rand : new Random();
        if (this.getItemDropped(state, rand, fortune) != Item.getItemFromBlock(this))
        {
            int i = 0;
            i = MathHelper.getInt(rand, 0, 5);
            return i;
        }
        return 0;
    }
    @Override
    protected boolean canSilkHarvest() {
        return true;
    }

    // spawn the actual item
    @Override
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state) {
        return new ItemStack(dropItem);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        Random rand = new Random();
        IBlockState iblockstate = this.getDefaultState();
        iblockstate = iblockstate.withProperty(FACING, directions[meta]).withProperty(TYPE,rand.nextInt(4));
        return iblockstate;
    }

    @Override
    public int getMetaFromState(IBlockState state)
    {
        switch (state.getValue(FACING)) {
            case UP:
                return 0;
            case DOWN:
                return 1;
            case NORTH:
                return 2;
            case SOUTH:
                return 3;
            case WEST:
                return 4;
            case EAST:
                return 5;
        }
        return 0;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, TYPE, FACING);
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        Random rand = world.rand;
        return this.getDefaultState().withProperty(FACING, facing).withProperty(TYPE,rand.nextInt(4));
    }

    @Override
    public boolean isPassable(IBlockAccess worldIn, BlockPos pos) {
        return true;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) { return false; }

    @Override
    @Nullable
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
    {
        return NULL_AABB;
    }
}