package net.paradisemod.world;

import net.paradisemod.base.Utils;
import net.paradisemod.world.blocks.Crystal;
import net.minecraft.init.Items;
import net.paradisemod.misc.Misc;

public class Crystals {
	public static Crystal diamondCrystal = new Crystal("diamond_crystal","diamondCrystal",Items.DIAMOND);
	public static Crystal emeraldCrystal = new Crystal("emerald_crystal","emeraldCrystal",Items.EMERALD);
	public static Crystal quartzCrystal = new Crystal("quartz_crystal", "quartzCrystal",Items.QUARTZ);
	public static Crystal redstoneCrystal = new Crystal("redstone_crystal","redstoneCrystal",Items.REDSTONE);
	public static Crystal rubyCrystal = new Crystal("ruby_crystal","rubyCrystal", Misc.Ruby);
	public static Crystal saltCrystal =  new Crystal("salt_crystal","saltCrystal", Misc.salt);

	public static void init() {
		Utils.regBlock(diamondCrystal);
		Utils.regBlock(emeraldCrystal);
		Utils.regBlock(quartzCrystal);
		Utils.regBlock(redstoneCrystal);
		Utils.regBlock(rubyCrystal);
		Utils.regBlock(saltCrystal);
	}

	public static void regRenders() {
		Utils.regRender(diamondCrystal);
		Utils.regRender(emeraldCrystal);
		Utils.regRender(quartzCrystal);
		Utils.regRender(redstoneCrystal);
		Utils.regRender(rubyCrystal);
		Utils.regRender(saltCrystal);
	}
}
