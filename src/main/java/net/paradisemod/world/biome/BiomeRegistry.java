package net.paradisemod.world.biome;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.BiomeProperties;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.common.BiomeManager.BiomeEntry;
import net.minecraftforge.common.BiomeManager.BiomeType;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class BiomeRegistry {
	public static final Biome glacier = new BiomeGlacier(new BiomeProperties("Glacier").setTemperature(-1f).setRainfall(0f).setSnowEnabled());
	public static final Biome iceShelf = new BiomeGlacier(new BiomeProperties("Ice Shelf").setTemperature(-1f).setRainfall(0f).setSnowEnabled());

	public static final Biome rockyDesert = new BiomeRockyDesert(new BiomeProperties("Rocky Desert").setTemperature(2f).setRainfall(0f).setHeightVariation(0f));
	public static final Biome rockyDesertHills = new BiomeRockyDesert( new BiomeProperties("Rocky Desert Hills").setTemperature(2f).setRainfall(0f).setBaseHeight(0.45F).setHeightVariation(0.3F));

	public static final Biome coldRockyDesert = new BiomeRockyDesert(new BiomeProperties("Cold Rocky Desert").setTemperature(0.25F).setHeightVariation(0f));
	public static final Biome coldRockyDesertHills = new BiomeRockyDesert( new BiomeProperties("Cold Rocky Desert Hills").setTemperature(0.25F).setBaseHeight(0.45F).setHeightVariation(0.3F));

	public static final Biome snowyRockyDesert = new BiomeRockyDesert(new BiomeProperties("Snowy Rocky Desert").setTemperature(-0.25F).setHeightVariation(0f));
	public static final Biome snowyRockyDesertHills = new BiomeRockyDesert( new BiomeProperties("Snowy Rocky Desert Hills").setTemperature(-0.25F).setBaseHeight(0.45F).setHeightVariation(0.3F));

	public static final Biome saltFlat= new BiomeSaltFlat(new BiomeProperties("Salt Flat").setTemperature(2f).setRainfall(0f).setBaseHeight(0F).setHeightVariation(-.01f).setRainDisabled());

	public static final Biome temperateJungle = new BiomeTemperateJungle(new BiomeProperties("Temperate Rainforest").setTemperature(0.5f).setRainfall(1f).setHeightVariation(0f));
	public static final Biome temperateJungleHills = new BiomeTemperateJungle(new BiomeProperties("Temperate Rainforest Hills").setTemperature(0.5f).setRainfall(1f).setBaseHeight(0.45F).setHeightVariation(0.3F));

	public static final Biome volcanicField = new BiomeVolcanic(new BiomeProperties("Volcanic Field").setTemperature(4f).setRainfall(0f).setHeightVariation(0f).setRainDisabled());
	public static final Biome volcanicMountains = new BiomeVolcanic(new BiomeProperties("Volcanic Mountains").setTemperature(4f).setRainfall(0f).setBaseHeight(1.0F).setHeightVariation(0.5F).setRainDisabled());
	
	public static void regBiomes() {
		initBiome(glacier, "glacier", BiomeType.ICY, Type.COLD);
		initBiome(iceShelf, "ice_shelf", BiomeType.ICY, Type.COLD,Type.BEACH,Type.RARE);

		initBiome(rockyDesert, "rocky_desert", BiomeType.DESERT,Type.DRY,Type.HOT);
		initBiome(rockyDesertHills, "rocky_desert_hills", BiomeType.DESERT,Type.DRY,Type.HOT);

		initBiome(coldRockyDesert, "cold_rocky_desert", BiomeType.COOL,Type.DRY,Type.COLD);
		initBiome(coldRockyDesertHills, "cold_rocky_desert_hills", BiomeType.COOL,Type.DRY,Type.COLD);

		initBiome(snowyRockyDesert, "snowy_rocky_desert", BiomeType.ICY,Type.DRY,Type.COLD);
		initBiome(snowyRockyDesertHills, "snowy_rocky_desert_hills", BiomeType.ICY,Type.DRY,Type.COLD);

		initBiome(saltFlat, "salt_flat", BiomeType.DESERT,Type.DRY,Type.HOT);

		initBiome(volcanicField, "volcanic_field", BiomeType.DESERT,Type.DRY, Type.HOT);
		initBiome(volcanicMountains, "volcanic_mountains", BiomeType.DESERT, Type.DRY, Type.HOT);

		initBiome(temperateJungle, "temperate_jungle", BiomeType.COOL, Type.COLD,Type.JUNGLE,Type.FOREST);
		initBiome(temperateJungleHills, "temperate_jungle_hills", BiomeType.COOL, Type.COLD,Type.JUNGLE,Type.FOREST);
	}
	
	public static Biome initBiome(Biome biome, String name, BiomeType biomeType, Type... types)
	{
		biome.setRegistryName(name);
		ForgeRegistries.BIOMES.register(biome);
		BiomeDictionary.addTypes(biome, types);
		BiomeManager.addBiome(biomeType, new BiomeEntry(biome, 10));
		BiomeManager.addSpawnBiome(biome);

		return biome;
	}
}