package net.paradisemod.world.biome;

import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockOldLeaf;
import net.minecraft.block.BlockOldLog;
import net.minecraft.block.BlockPlanks;
import net.minecraft.entity.passive.EntityOcelot;
import net.minecraft.init.Blocks;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.WorldGenAbstractTree;
import net.minecraft.world.gen.feature.WorldGenTaiga1;
import net.minecraft.world.gen.feature.WorldGenTrees;

import java.util.Random;

public class BiomeTemperateJungle  extends Biome {
	public static final WorldGenAbstractTree birch = new WorldGenTrees(false,3,Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.BIRCH),Blocks.LEAVES.getDefaultState()
			.withProperty(BlockOldLeaf.VARIANT, BlockPlanks.EnumType.BIRCH)
			.withProperty(BlockLeaves.CHECK_DECAY, Boolean.valueOf(false))
			.withProperty(BlockLeaves.DECAYABLE, Boolean.valueOf(true)),true);
	
	public static final WorldGenAbstractTree spruce = new WorldGenTaiga1();

	public static final WorldGenAbstractTree jungle = new WorldGenTrees(false,3,Blocks.LOG.getDefaultState().withProperty(BlockOldLog.VARIANT, BlockPlanks.EnumType.JUNGLE),Blocks.LEAVES.getDefaultState()
			.withProperty(BlockOldLeaf.VARIANT, BlockPlanks.EnumType.JUNGLE)
			.withProperty(BlockLeaves.CHECK_DECAY, Boolean.valueOf(false))
			.withProperty(BlockLeaves.DECAYABLE, Boolean.valueOf(true)),true);

	public BiomeTemperateJungle(BiomeProperties properties) {
		super(properties);
		decorator.treesPerChunk = 50;
		decorator.grassPerChunk = 25;
		decorator.flowersPerChunk = 5;
		decorator.cactiPerChunk = 0;
		decorator.clayPerChunk = 1;
		decorator.deadBushPerChunk = 0;
		decorator.mushroomsPerChunk = 3;
		topBlock = Blocks.GRASS.getDefaultState();
		fillerBlock = Blocks.DIRT.getDefaultState();

		// just like vanilla jungles, ocelots spawn here too
		spawnableMonsterList.add(new Biome.SpawnListEntry(EntityOcelot.class, 2, 1, 1));
	}

	@Override
	public WorldGenAbstractTree getRandomTreeFeature(Random rand) {
		int tree = rand.nextInt(3);
		switch (tree) {
			case 0:
				return birch;
			case 1:
				return spruce;
			case 2:
				return jungle;
		}
		return jungle;
	}
}