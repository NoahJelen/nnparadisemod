package net.paradisemod.automation;

import net.paradisemod.ParadiseMod;
import net.paradisemod.base.Utils;
import net.paradisemod.automation.blocks.MossyFurnace;
import net.paradisemod.automation.tileentity.TileEntityMossyFurnace;
import net.paradisemod.automation.tileentity.TileEntityVoidFurnace;
import net.paradisemod.automation.blocks.VoidFurnace;
import net.paradisemod.automation.blocks.GoldHopper;
import net.paradisemod.automation.blocks.SilverHopper;
import net.paradisemod.automation.tileentity.TileEntityGoldHopper;
import net.paradisemod.automation.tileentity.TileEntitySilverHopper;
import net.paradisemod.automation.blocks.emeraldRail;
import net.paradisemod.automation.blocks.emeraldRailPowered;
import net.minecraft.block.BlockHopper;
import net.minecraft.block.BlockRail;
import net.minecraft.block.BlockRailPowered;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.apache.logging.log4j.Level;

public class Automation {
    //furnaces
    public static net.paradisemod.automation.blocks.MossyFurnace MossyFurnace = new MossyFurnace(false);
    public static net.paradisemod.automation.blocks.MossyFurnace MossyFurnaceLit = new MossyFurnace(true);
    public static net.paradisemod.automation.blocks.VoidFurnace VoidFurnace = new VoidFurnace(false);
    public static net.paradisemod.automation.blocks.VoidFurnace VoidFurnaceLit = new VoidFurnace(true);

    //hoppers
    public static BlockHopper GoldHopper = new GoldHopper();
    public static BlockHopper SilverHopper = new SilverHopper();

    //rails
    public static BlockRail emeraldRail = new emeraldRail();
    public static BlockRailPowered emeraldRailPowered = new emeraldRailPowered();

    public static void init() {
        Utils.regBlock(MossyFurnace.setUnlocalizedName("MossyFurnace").setRegistryName("mossy_furnace").setHardness(5F).setResistance(10F).setCreativeTab(CreativeTabs.DECORATIONS));
        ForgeRegistries.BLOCKS.register(MossyFurnaceLit.setUnlocalizedName("MossyFurnaceLit").setRegistryName("lit_mossy_furnace").setHardness(5F).setResistance(10F));
        Utils.regBlock(VoidFurnace.setUnlocalizedName("VoidFurnace").setRegistryName("void_furnace").setHardness(5F).setResistance(10F).setCreativeTab(CreativeTabs.DECORATIONS));
        ForgeRegistries.BLOCKS.register(VoidFurnaceLit.setUnlocalizedName("VoidFurnaceLit").setRegistryName("lit_void_furnace").setHardness(5F).setResistance(10F));

        Utils.regBlock(SilverHopper);
        Utils.regBlock(GoldHopper);

        // hopper tile entities
        GameRegistry.registerTileEntity(TileEntityGoldHopper.class,"gold_hopper");
        GameRegistry.registerTileEntity(TileEntitySilverHopper.class,"silver_hopper");

        // furnace tile entities
        GameRegistry.registerTileEntity(TileEntityMossyFurnace.class,"mossy_furnace");
        GameRegistry.registerTileEntity(TileEntityVoidFurnace.class,"void_furnace");

        Utils.regBlock(emeraldRail);
        Utils.regBlock(emeraldRailPowered);

        ParadiseMod.LOG.log(Level.INFO,"Loaded automation module");
    }

    public static void regRenders(){
        Utils.regRender(MossyFurnace);
        Utils.regRender(VoidFurnace);
        Utils.regRender(SilverHopper);
        Utils.regRender(GoldHopper);
        Utils.regRender(emeraldRailPowered);
        Utils.regRender(emeraldRail);
    }
}
