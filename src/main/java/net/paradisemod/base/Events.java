package net.paradisemod.base;

import com.google.common.collect.Lists;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.registries.IForgeRegistryModifiable;
import net.paradisemod.bonus.Bonus;

import java.util.ArrayList;

public class Events {
	// replaces the default recipes for crafting table, wooden pressure plate, wooden button, and furnace with my own
	@SubscribeEvent
	public static void deleteRecipes(RegistryEvent.Register<IRecipe> event) {
		ResourceLocation woodenPressurePlate = new ResourceLocation("minecraft:wooden_pressure_plate");
		ResourceLocation oakTable = new ResourceLocation("minecraft:crafting_table");
		ResourceLocation woodenButton = new ResourceLocation("minecraft:wooden_button");
		ResourceLocation furnace = new ResourceLocation("minecraft:furnace");
		@SuppressWarnings("rawtypes")
		IForgeRegistryModifiable modRegistry = (IForgeRegistryModifiable) event.getRegistry();
		modRegistry.remove(woodenPressurePlate);
		modRegistry.remove(woodenButton);
		modRegistry.remove(oakTable);
		modRegistry.remove(furnace);
	}

	// unlocks all recipes if Quark is not present
	@SubscribeEvent
	public static void onPlayerLoggedIn(PlayerLoggedInEvent event) {
		if (event.player instanceof EntityPlayerMP) {
			ArrayList<IRecipe> recipes = Lists.newArrayList(CraftingManager.REGISTRY);
			event.player.unlockRecipes(recipes);
		}
	}

	@SubscribeEvent
	public static void maskedVillager(EntityEvent event) {
		Entity entity = event.getEntity();
		if (!(entity instanceof EntityVillager)) return;
		EntityVillager villager = (EntityVillager) entity;

		if (villager.getCustomNameTag().equals("Doomer") || villager.getCustomNameTag().equals("doomer")) {
			if (villager.getItemStackFromSlot(EntityEquipmentSlot.HEAD).getItem() != Bonus.faceDiaper) {
				villager.tasks.addTask(1, new EntityAIAvoidEntity(villager, EntityVillager.class, 2.0F, 0.6D, 0.6D));
				villager.tasks.addTask(1, new EntityAIAvoidEntity(villager, EntityPlayer.class, 2.0F, 0.6D, 0.6D));
				villager.tasks.addTask(1, new EntityAIAvoidEntity(villager, EntityPlayerMP.class, 2.0F, 0.6D, 0.6D));
				villager.setItemStackToSlot(EntityEquipmentSlot.HEAD, new ItemStack(Bonus.faceDiaper));
			}
		}
	}
}