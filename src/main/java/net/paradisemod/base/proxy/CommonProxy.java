package net.paradisemod.base.proxy;

public interface CommonProxy {
	public void init();
	public boolean isClient();
}