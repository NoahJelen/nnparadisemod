package net.paradisemod.base;

import net.paradisemod.bonus.Bonus;
import net.paradisemod.building.Building;
import net.paradisemod.world.Ores;
import net.paradisemod.misc.Misc;
import net.minecraftforge.oredict.OreDictionary;

public class OreDictHandler {
	public static void registerOreDict() {
		OreDictionary.registerOre("oreRuby", Ores.RubyOre);
		OreDictionary.registerOre("blockRuby", Ores.RubyBlock);
		OreDictionary.registerOre("gemRuby", Misc.Ruby);
		OreDictionary.registerOre("saplingChristmas", Bonus.ChristmasSapling);
		OreDictionary.registerOre("leavesChristmas", Bonus.ChristmasLeaves);
		OreDictionary.registerOre("oreSilver", Ores.SilverOre);
		OreDictionary.registerOre("oreSilver", Ores.SilverOreNether);
		OreDictionary.registerOre("blockSilver", Ores.SilverBlock);
		OreDictionary.registerOre("ingotSilver", Misc.SilverIngot);
		OreDictionary.registerOre("nuggetSilver", Misc.SilverNugget);
		OreDictionary.registerOre("stickWood", Misc.CactusStick);
		OreDictionary.registerOre("plankWood",Building.CompressedCactus);
		OreDictionary.registerOre("dustSalt", Misc.salt);
		OreDictionary.registerOre("oreSalt", Ores.SaltOre);
		OreDictionary.registerOre("blockSalt", Ores.SaltBlock);
		OreDictionary.registerOre("lampSalt", Misc.SaltLamp);
		OreDictionary.registerOre("oreGold", Ores.GoldOreNether);
		OreDictionary.registerOre("ingotRustyIron", Misc.RustyIngot);
		OreDictionary.registerOre("blockRustyIron", Ores.RustyIronBlock);
		OreDictionary.registerOre("nuggetRustyIron", Misc.RustyNugget);
		OreDictionary.registerOre("dyeBlack", Misc.BlackDye);
		OreDictionary.registerOre("dyeBrown", Misc.BrownDye);
		OreDictionary.registerOre("dyeBlue", Misc.BlueDye);
		OreDictionary.registerOre("cobblestone", Building.VoidStone);
	}
}